package DTO;

import java.util.*;

//dto luong
public class SalaryDTO {
	private String idSalary;
	private int standardSalary;// luong can ban
	private static List<SalaryDTO> salaryList = new ArrayList<SalaryDTO>();

	public SalaryDTO(String idSalary, int standardSalary) {
		super();
		this.idSalary = idSalary;
		this.standardSalary = standardSalary;
	}

	public SalaryDTO() {
		super();
	}

	public String getIdSalary() {
		return idSalary;
	}

	public void setIdSalary(String idSalary) {
		this.idSalary = idSalary;
	}

	public int getStandardSalary() {
		return standardSalary;
	}

	public void setStandardSalary(int standardSalary) {
		this.standardSalary = standardSalary;
	}

	public static List<SalaryDTO> getSalaryList() {
		return salaryList;
	}

	public static void setSalaryList(List<SalaryDTO> salaryList) {
		SalaryDTO.salaryList = salaryList;
	}
}
