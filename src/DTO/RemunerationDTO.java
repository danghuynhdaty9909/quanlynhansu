package DTO;

import java.util.*;

//dto thuong
public class RemunerationDTO {
	private String idRemuneration;
	private String remunerationName;
	private double level; // muc thuong
	private static List<RemunerationDTO> remunerationList = new ArrayList<RemunerationDTO>();

	public RemunerationDTO() {
		super();
	}

	public RemunerationDTO(String idRemuneration, String remunerationName, double level) {
		super();
		this.idRemuneration = idRemuneration;
		this.remunerationName = remunerationName;
		this.level = level;
	}

	public String getIdRemuneration() {
		return idRemuneration;
	}

	public void setIdRemuneration(String idRemuneration) {
		this.idRemuneration = idRemuneration;
	}

	public String getRemunerationName() {
		return remunerationName;
	}

	public void setRemunerationName(String remunerationName) {
		this.remunerationName = remunerationName;
	}

	public double getLevel() {
		return level;
	}

	public void setLevel(double level) {
		this.level = level;
	}

	public static List<RemunerationDTO> getRemunerationList() {
		return remunerationList;
	}

	public static void setRemunerationList(List<RemunerationDTO> remunerationList) {
		RemunerationDTO.remunerationList = remunerationList;
	}

}
