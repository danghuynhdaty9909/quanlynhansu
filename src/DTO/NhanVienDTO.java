package DTO;

import java.util.*;

public class NhanVienDTO {
	private String id;
	private String firstName;
	private String lastName;
	private String sex;
	private String address;
	private String nation; // dan toc
	private String phoneNumber;
	private String account;
	private String birthday;
	private String idDepartment;
	private String idContract; // ma hop dong
	private String idPosition;
	private String idLiteracy; // ma trinh do hoc van
	private String mail;
	private String idCard;// chung minh nhan dan;
	private String pathImg;
	private List<NhanVienDTO> nhanvienList = new ArrayList<NhanVienDTO>();

	public NhanVienDTO(String id, String firstName, String lastName, String sex, String address, String nation,
			String phoneNumber, String account, String birthday, String idDepartment, String idContract,
			String idPosition, String idLiteracy, String mail, String idCard, String pathImg) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.sex = sex;
		this.address = address;
		this.nation = nation;
		this.phoneNumber = phoneNumber;
		this.account = account;
		this.birthday = birthday;
		this.idDepartment = idDepartment;
		this.idContract = idContract;
		this.idPosition = idPosition;
		this.idLiteracy = idLiteracy;
		this.mail = mail;
		this.idCard = idCard;
		this.pathImg = pathImg;
	}

	public NhanVienDTO() {
		super();
	}
	public String getId() {
		return id;
	}
	public void setId(String id){
		this.id=id;
	}
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getIdDepartment() {
		return idDepartment;
	}

	public void setIdDepartment(String idDepartment) {
		this.idDepartment = idDepartment;
	}

	public String getIdContract() {
		return idContract;
	}

	public void setIdContract(String idContract) {
		this.idContract = idContract;
	}

	public String getIdPosition() {
		return idPosition;
	}

	public void setIdPosition(String idPosition) {
		this.idPosition = idPosition;
	}

	public String getIdLiteracy() {
		return idLiteracy;
	}

	public void setIdLiteracy(String idLiteracy) {
		this.idLiteracy = idLiteracy;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getPathImg() {
		return pathImg;
	}

	public void setPathImg(String pathImg) {
		this.pathImg = pathImg;
	}

	public List<NhanVienDTO> getNhanvienList() {
		return nhanvienList;
	}

	public void setNhanvienList(List<NhanVienDTO> nhanvienList) {
		this.nhanvienList = nhanvienList;
	}

	public String toString() {
		StringBuilder st = new StringBuilder();
		st.append(getId()).append(getFirstName()).append(getLastName()).append(getSex());
		st.append(getBirthday()).append(getAddress()).append(getNation()).append(getIdCard()).append(getPhoneNumber());
		st.append(getMail()).append(getAccount()).append(getPathImg()).append(getIdContract()).append(getIdPosition());
		st.append(getIdDepartment()).append(getIdLiteracy());
		return st.toString();
	}
}
