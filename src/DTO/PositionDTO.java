package DTO;

import java.util.*;

//dto chuc vu
public class PositionDTO {
	private String idPosition;
	private String positionName;
	private static List<PositionDTO> positionList = new ArrayList<PositionDTO>();

	public PositionDTO(String idPosition, String positionName) {
		super();
		this.idPosition = idPosition;
		this.positionName = positionName;
	}

	public PositionDTO() {
		super();
	}

	public String getIdPosition() {
		return idPosition;
	}

	public void setIdPosition(String idPosition) {
		this.idPosition = idPosition;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public List<PositionDTO> getPositionList() {
		return positionList;
	}

	@SuppressWarnings("static-access")
	public void setPositionList(List<PositionDTO> positionList) {
		this.positionList = positionList;
	}

}
