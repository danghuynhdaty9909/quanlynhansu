package DTO;

import java.util.*;

//dto trinh do hoc van
public class LiteracyDTO {
	private String idLiteracy;
	private String literacyName;
	private String specialization; // chuyen nganh;
	private static List<LiteracyDTO> literacyList = new ArrayList<LiteracyDTO>();

	public LiteracyDTO(String idLiteracy, String literacyName, String specialization) {
		super();
		this.idLiteracy = idLiteracy;
		this.literacyName = literacyName;
		this.specialization = specialization;
	}

	public LiteracyDTO() {
		super();
	}

	public String getIdLiteracy() {
		return idLiteracy;
	}

	public void setIdLiteracy(String idLiteracy) {
		this.idLiteracy = idLiteracy;
	}

	public String getLiteracyName() {
		return literacyName;
	}

	public void setLiteracyName(String literacyName) {
		this.literacyName = literacyName;
	}

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public List<LiteracyDTO> getLiteracyList() {
		return literacyList;
	}

	@SuppressWarnings("static-access")
	public void setLiteracyList(List<LiteracyDTO> literacyList) {
		this.literacyList = literacyList;
	}

}
