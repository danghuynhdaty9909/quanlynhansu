package DTO;

public class UserDTO {

	protected String username;
	protected String password;

	// constructor
	public UserDTO(String username, String password) {
		this.username = username;
		this.password = password;
	}
	public UserDTO() {
	}

	// getter setter
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
