package DTO;

import java.util.ArrayList;
import java.util.List;

//dto hop dong
public class ContractDTO {
	private String idContract;
	private String typeContract;
	private String beginDay;
	private static List<ContractDTO> contractList = new ArrayList<ContractDTO>();

	public ContractDTO(String idContract, String typeContract, String beginDay) {
		super();
		this.idContract = idContract;
		this.typeContract = typeContract;
		this.beginDay = beginDay;
	}

	public ContractDTO() {
		super();
	}

	public String getIdContract() {
		return idContract;
	}

	public void setIdContract(String idContract) {
		this.idContract = idContract;
	}

	public String getTypeContract() {
		return typeContract;
	}

	public void setTypeContract(String typeContract) {
		this.typeContract = typeContract;
	}

	public String getBeginDay() {
		return beginDay;
	}

	public void setBeginDay(String beginDay) {
		this.beginDay = beginDay;
	}

	public static List<ContractDTO> getContractList() {
		return contractList;
	}

	public static void setContractList(List<ContractDTO> contractList) {
		ContractDTO.contractList = contractList;
	}

}
