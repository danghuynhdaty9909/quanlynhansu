package DTO;

import java.util.ArrayList;
import java.util.List;

//dto phong ban
public class DepartmentDTO {
	private String id;
	private String name = "";
	private String idBoss;
	private String address = "";
	private List<DepartmentDTO> departmentList = new ArrayList<DepartmentDTO>();

	// private int count;// số nhân viên;
	public DepartmentDTO(String id, String name, String idBoss, String address) {
		super();
		this.id = id;
		this.name = name;
		this.idBoss = idBoss;
		this.address = address;

	}

	public DepartmentDTO() {
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdBoss() {
		return idBoss;
	}

	public void setIdBoss(String idBoss) {
		this.idBoss = idBoss;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<DepartmentDTO> getDepartmentList() {
		return departmentList;
	}

	public void setDepartmentList(List<DepartmentDTO> departmentList) {
		this.departmentList = departmentList;
	}

}
