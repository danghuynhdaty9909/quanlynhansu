package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import DTO.ContractDTO;

public class HopDongLaoDongDAO {
	DataConnection dc = new DataConnection();

	public String getIdContractByName(String name) {
		String query = "select mahdld from quanlynhansu.hopdonglaodong where loaihopdong='" + name + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				return result.getString(1);
			}
		} catch (SQLException e) {
			dc.Disconnect();
			return "";
		}
		return "";
	}

	public String getName(String id) {
		String query = "select loaihopdong from quanlynhansu.hopdonglaodong where mahdld='" + id + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				String st = result.getString(1);
				dc.Disconnect();
				return st;
			}
		} catch (SQLException e) {
			dc.Disconnect();
			JOptionPane.showMessageDialog(null, "Lấy loại hợp đồng lỗi!!!");
			return "";
		}
		return "";
	}

	public boolean delete(String idContract) {
		String query = "delete from quanlynhansu.hopdonglaodong where mahdld='" + idContract + "'";
		dc.Connect();
		try {
			@SuppressWarnings("unused")
			int k = dc.con.createStatement().executeUpdate(query);
			dc.Disconnect();
			return true;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}

	public ArrayList<String> getListName() {// lây danh sách loại hợp đồng
		String query = "select distinct loaihopdong from quanlynhansu.hopdonglaodong";
		ArrayList<String> list = new ArrayList<>();
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				list.add(result.getString(1));
			}
			return list;
		} catch (SQLException e) {
			dc.Disconnect();
			JOptionPane.showMessageDialog(null, "Lấy loại hợp đồng lỗi!!!");
			return null;
		}
	}

	public ArrayList<String> getList(String col) {// lây danh sách
		String query = "select distinct " + col + " from quanlynhansu.hopdonglaodong";
		ArrayList<String> list = new ArrayList<>();
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				list.add(result.getString(1));
			}
			return list;
		} catch (SQLException e) {
			dc.Disconnect();
			JOptionPane.showMessageDialog(null, "Lấy loại hợp đồng lỗi!!!");
			return null;
		}
	}

	public boolean check(String name, String valuename, String day, String valueday) {
		String query = "select * from quanlynhansu.hopdonglaodong where " + name + "='" + valuename + "' and " + day
				+ "='" + valueday + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				dc.Disconnect();
				return true;
			}
			return false;
		} catch (SQLException e) {
			return false;
		}
	}

	public boolean insert(ContractDTO contract) {
		String query = "insert into `quanlynhansu`.`hopdonglaodong` (`mahdld`, `loaihopdong`, `ngaybatdau`) values('"
				+ contract.getIdContract() + "','" + contract.getTypeContract() + "','" + contract.getBeginDay() + "')";
		dc.Connect();
		try {
			int k = dc.con.createStatement().executeUpdate(query);
			dc.Disconnect();
			return k >= 1 ? true : false;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}

	public String createNewId() {
		int max = 0;
		ArrayList<String> list = this.getList("mahdld");
		if (!list.isEmpty()) {
			for (String st : list) {
				if (max < Integer.parseInt(st.substring(2))) {
					max = Integer.parseInt(st.substring(2));
				}
			}
		}
		max++;
		String id = "HD";
		if (max < 10) {
			id += "0" + String.valueOf(max);
		} else {
			id += String.valueOf(max);
		}
		return id;
	}
	@SuppressWarnings("unused")
	public boolean update(ContractDTO contract) {
		String query = "update quanlynhansu.hopdonglaodong set loaihopdong='" + contract.getTypeContract()
				+ "', ngaybatdau='" + contract.getBeginDay() + "'"+" where mahdld='"+contract.getIdContract()+"'";
		dc.Connect();
		try {
			int k = dc.con.createStatement().executeUpdate(query);
			dc.Disconnect();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}
}
