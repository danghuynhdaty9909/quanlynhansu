package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class ChucVuDAO {
	DataConnection dc=new DataConnection();
	public String getIdPositinByName(String name){
		String query="select macv from quanlynhansu.chucvu where tencv='"+name+"'";
		dc.Connect();
		try {
			ResultSet result=dc.con.createStatement().executeQuery(query);
			if(result.next()){
				return result.getString(1);
			}
		} catch (SQLException e) {
			dc.Disconnect();
			return "";
		}
		return "";
	}
	public String getNameById(String id){
		String query="select tencv from quanlynhansu.chucvu where macv='"+id+"'";
		dc.Connect();
		try {
			ResultSet result=dc.con.createStatement().executeQuery(query);
			if(result.next()){
				String st=result.getString(1);
				dc.Disconnect();
				return st;
			}
		} catch (SQLException e) {
			dc.Disconnect();
			JOptionPane.showMessageDialog(null, "Lấy tên chức vụ lỗi!!!");
			return "";
		}
		return "";
	}
	public ArrayList<String> getListNameByCond(String col,String cond, String value){//lấy danh sách 1 cột
		ArrayList<String> list = new ArrayList<>();
		String query = "select " + col + " from quanlynhansu.chucvu";
		if (!cond.isEmpty()) {
			query += " where " + cond + "='" + value + "'";
		}
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				list.add(result.getString(1));
			}
			dc.Disconnect();
			return list;
		} catch (SQLException e) {
			dc.Disconnect();
			JOptionPane.showMessageDialog(null, "ChucVuDAO thông báo:Lỗi khi lấy danh sách!!!");
			return null;
		}
	}
}
