package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ThuongDAO {
	DataConnection dc = new DataConnection();
	
	//hàm lấy về mức thưởng dựa vào mã thưởng
	public int getMucThuong(String maThuong)
	{
		int result = 0;
		try {
			dc.Connect();
			String sql = "SELECT mucthuong FROM thuong WHERE mathuong=? ";
			PreparedStatement ps = dc.con.prepareStatement(sql);
			ps.setString(1, maThuong);
			
			ResultSet rs = ps.executeQuery();
			
			result = rs.getInt(1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		dc.Disconnect();
		
		return result;
		
		
	}
	
}
