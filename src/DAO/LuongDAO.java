package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LuongDAO {
	DataConnection dc = new DataConnection();
	//function get luong co ban tu ma bac luong
	public int getLuong(String mabacluong)
	{
		int bacLuong = 0;
		dc.Connect();
		String sql = "select * from luong where mabacluong='"+mabacluong+"'";
		try {
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			
			if(rs.next()){
				bacLuong = rs.getInt(2);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			dc.Disconnect();
		}
		return bacLuong;
	}
	
	public static void main(String[] args) {
		LuongDAO l = new LuongDAO();
		int rs = l.getLuong("1");
		System.out.println(rs);
	}
}
