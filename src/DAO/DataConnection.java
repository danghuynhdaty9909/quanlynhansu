package DAO;

import java.sql.*;

import javax.swing.JOptionPane;

public class DataConnection {

	protected Connection con = null;

	//hàm kết nối database
	public Connection Connect() {
		
		//nếu chua co ket noi thi mo ket noi
		if (con == null) {
			final String URL = "jdbc:mysql://localhost:3306/quanlynhansu?useSSL=true";
			final String PASSWORD = "khongcopass";
			final String USERNAME = "root";

			try {
				Class.forName("com.mysql.jdbc.Driver");

				con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
				
				//thành công thì trả v�? biến con
				return con;
			} catch (ClassNotFoundException e) {
				JOptionPane.showMessageDialog(null, "Kết nối thất bại!!");
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Kết nối thất bại!!");
			}		
		}
		
		//nếu không thành công trả ve null
		return null;
	}

	//hàm đóng kết nối database
	public void Disconnect()
	{
		//nếu chưa đóng kết nối thì thực hiện việc đóng kết nối
		if(con!=null)
		{
			try {
				con.close();
				con = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	
	
}
