package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import DTO.DepartmentDTO;
import DTO.NhanVienDTO;

public class PhongBanDAO {
	DataConnection dc = new DataConnection();

	public String getIdDepartment(String name) {
		String query = "select maphongban from quanlynhansu.phongban where tenphong='" + name + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {

				return result.getString(1);
			}
		} catch (SQLException e) {
			return "";
		}
		return "";
	}

	public String getNameDepartment(String id) {
		String query = "select tenphong from quanlynhansu.phongban where maphongban='" + id + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				return result.getString(1);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Lấy tên phòng lỗi!!!");
			dc.Disconnect();
			return "";
		}
		return "";
	}

	public int countStaffByDepartment(String idDepartment) {
		dc.Connect();
		String query = "select count(manv) from quanlynhansu.nhanvien where maphongban='" + idDepartment + "'";
		ResultSet result = null;
		try {
			result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				int k = Integer.parseInt(result.getString(1));
				return k;
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Đã xảy ra lỗi!!!");
			dc.Disconnect();
			return -1;
		}
		return -1;
	}

	public ArrayList<DepartmentDTO> loadData() {
		ArrayList<DepartmentDTO> arr = new ArrayList<DepartmentDTO>();
		String query = "select * from quanlynhansu.phongban";
		dc.Connect();
		ResultSet result = null;
		try {
			result = dc.con.createStatement().executeQuery(query);
			DepartmentDTO department;
			while (result.next()) {
				department = new DepartmentDTO();
				department.setId(result.getString(1));
				department.setName(result.getString(2));
				department.setIdBoss(result.getString(3));
				department.setAddress(result.getString(4));
				arr.add(department);
			}
			dc.Disconnect();
			return arr;

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Đã xảy ra lỗi!!!");
			dc.Disconnect();
			return null;
		}
	}

	public String getNameBoss(String id) {
		String query = "select ho,ten from quanlynhansu.nhanvien where manv='" + id + "'";
		dc.Connect();
		ResultSet result = null;
		String s = "";
		try {
			result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				s = result.getString(1) + " " + result.getString(2);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Đã xảy ra lỗi!!!!");
		}
		dc.Disconnect();
		return s;
	}

	public ArrayList<String> getColByCondition( String cond, String value) {
		String query = "select * from quanlynhansu.phongban where " + cond + "='" + value + "'";
		dc.Connect();
		ResultSet result=null;
		ArrayList< String > list=new ArrayList<>();
		try {
			result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				list.add(result.getString(1));
				list.add(result.getString(2));
				list.add(result.getString(3));
				list.add(result.getString(4));
				dc.Disconnect();
				return list;
			}
			dc.Disconnect();
			return null;
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
	}

	public boolean update(DepartmentDTO dto) {// sửa
												// phòng
												// ban
												// theo
												// id
												// phòng
												// ban
		String query = "update quanlynhansu.phongban set tenphong='" + dto.getName() + "',diachi='" + dto.getAddress()
				+ "',truongphong='" + dto.getIdBoss() + "' where maphongban='" + dto.getId() + "'";
		dc.Connect();
		try {
			int k = dc.con.createStatement().executeUpdate(query);
			if (k >= 1) {
				return true;
			}
			return true;
		} catch (SQLException e) {
			return false;
		}
	}

	public ArrayList<String> getListOneCol(String col, String cond, String value) {// lấy
																					// danh
																					// sách
																					// một
																					// cột
		ArrayList<String> list = new ArrayList<>();
		String query = "select " + col + " from quanlynhansu.phongban";
		if (!cond.isEmpty()) {
			query += " where " + cond + "='" + value + "'";
		}
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				list.add(result.getString(1));
			}
			dc.Disconnect();
			return list;
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}

	}

	public ArrayList<NhanVienDTO> getList() {// lấy danh sách nhan viên để chọn
												// trưởng phòng
		ArrayList<NhanVienDTO> list = new ArrayList<>();
		String query = "select * from quanlynhansu.nhanvien";
		dc.Connect();
		ResultSet result = null;
		try {
			result = dc.con.createStatement().executeQuery(query);
			NhanVienDTO staff;
			while (result.next()) {
				staff = new NhanVienDTO(result.getString(1), result.getString(2), result.getString(3),
						result.getString(4), result.getString(6), result.getString(7), result.getString(9),
						result.getString(11), result.getString(5), result.getString(15), result.getString(13),
						result.getString(14), result.getString(16), result.getString(10), result.getString(8),
						result.getString(12));
				list.add(staff);
			}
			dc.Disconnect();
			return list;
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Đã xảy ra lỗi!!!");
			dc.Disconnect();
			return null;
		}
	}

	public boolean insert(DepartmentDTO depart) {
		StringBuilder query = new StringBuilder();
		query.append("insert into quanlynhansu.phongban values('");
		query.append(depart.getId() + "','" + depart.getName() + "','" + depart.getIdBoss() + "','"
				+ depart.getAddress() + "')");
		dc.Connect();
		try {
			int k = dc.con.createStatement().executeUpdate(query.toString());
			return k >= 1 ? true : false;
		} catch (SQLException e) {
			return false;
		}
	}
	public ResultSet countStaff(){
		String query="select maphongban,count(manv) from quanlynhansu.nhanvien group by maphongban";
		dc.Connect();
		try {
			ResultSet result=dc.con.createStatement().executeQuery(query);
			return result;
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
		
	}
	public String getAddress(String id){
		String query="select diachi from quanlynhansu.phongban where maphongban='"+id+"'";
		dc.Connect();
		ResultSet result=null;
		try {
			result=dc.con.createStatement().executeQuery(query);
			if(result.next()){
				String address=result.getString(1);
				dc.Disconnect();
				return address;
			}
			dc.Disconnect();
			return "";
		} catch (SQLException e) {
			dc.Disconnect();
			return "";
		}
	}
	public String getIdBoss(String idDepart){
		String query="select truongphong from quanlynhansu.phongban where maphongban='"+idDepart+"'";
		dc.Connect();
		ResultSet result=null;
		try {
			result=dc.con.createStatement().executeQuery(query);
			if(result.next()){
				String idBoss=result.getString(1);
				dc.Disconnect();
				return idBoss;
			}
			dc.Disconnect();
			return "";
		} catch (SQLException e) {
			dc.Disconnect();
			return "";
		}
	}
}
