package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO {

	DataConnection DC = new DataConnection();
	
	public boolean login(String username, String password)
	{
		boolean result = false;
		
		
		try {
			DC.Connect();
			
			String sql = "SELECT * FROM user WHERE username=? AND password=?";
			PreparedStatement ps= DC.con.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			
			ResultSet rs = ps.executeQuery();
			if(rs!=null && rs.next())
			{
				result = true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
		DC.Disconnect();
		return result;
	}
	
	
	
}
