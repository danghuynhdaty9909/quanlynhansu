package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ChamCongDAO {
	DataConnection dc = new DataConnection();
	public int[] getSoGioLam(String maChamCong)
	{
		int[] soGioLam =new int[2];
		soGioLam[0]=0;
		soGioLam[1]=0;
		dc.Connect();
		String sql = "select * from chamcong where machamcong='"+maChamCong+"'";
		try {
			
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			String str = "";
			String[] res;
			if(rs!=null && rs.next()){
				for(int i = 3;i<=33;i++)
				{
					str = rs.getString(i);
					res = str.split("-");//cắt chuỗi lấy ra số giờ làm
					soGioLam[0]+= Integer.parseInt(res[0]);//cast để lấy số giờ làm bình thường
					soGioLam[1]+= Integer.parseInt(res[1]);//cast để lấy số giờ làm tăng ca
				}
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return soGioLam;
	}
	
	//tính số ngày làm
	public int soNgayLam(String maNhanVien, String thang)
	{
		int soNgayLam=0;
		
		dc.Connect();
		String sql = "select * from luongchitiet,chamcong where manv='"+maNhanVien+"' and chamcong.thang='"+thang+"' and chamcong.machamcong=luongchitiet.machamcong";
		try {
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			if(rs.next() && rs!=null)
			{
				for(int i=10;i<=40;i++)
				{
					if(!rs.getString(i).equals("0-0"))
					{
						soNgayLam++;
					}
					
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return soNgayLam;
	}
	
	public static void main(String[] args) {
		ChamCongDAO c = new ChamCongDAO();
		System.out.println(c.soNgayLam("18","4"));
	}
	
}
