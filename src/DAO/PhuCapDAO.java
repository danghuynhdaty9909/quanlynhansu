package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PhuCapDAO {
	DataConnection dc = new DataConnection();

	// hàm tính phụ cấp dựa vào mã phụ cấp
	public double getPhuCap(String maphucap) {
		double pc = 0;
		dc.Connect();
		String sql = "select hesophucap from phucap where maphucap='"+maphucap+"';";
		try {
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			
			if(rs.next())
			{
				pc = rs.getDouble(1);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			dc.Disconnect();
		}

		return pc;

	}

	public static void main(String[] args) {
		PhuCapDAO pc = new PhuCapDAO();
		System.out.println(pc.getPhuCap("3"));
	}

}
