package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LuongChiTietDAO {
	DataConnection dc = new DataConnection();

	public boolean TinhLuongTheoNhanVien(int thang, String maNhanVien) {
		/*
		 * a=(luongcoban+phucap /24/8)
		 * luong = a*sogiolambinhthuong +a*1.5*sogiolamtangca+thuong
		 */
		boolean flag = false;
		dc.Connect();
		int[]soGioLam;
		ChamCongDAO chamCong = new ChamCongDAO();
		String sql = "select * from luongchitiet,chamcong,phucap,thuong,luong where manv='"+maNhanVien+"' and thang="+thang+" and luong.mabacluong = luongchitiet.mabacluong and thuong.mathuong=luongchitiet.mathuong and phucap.maphucap=luongchitiet.maphucap  and chamcong.machamcong=luongchitiet.machamcong;";
		try {
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			if(rs.next()&&rs!=null)
			{
				flag = true;
				int luong=(int) (rs.getInt("luongcoban")+(rs.getInt("luongcoban")*rs.getDouble("hesophucap")))/24/8;
				soGioLam = chamCong.getSoGioLam(rs.getString("luongchitiet.machamcong"));
				luong = (int) (luong*soGioLam[0]+luong*1.5*soGioLam[1]+rs.getInt("thuong.mucthuong"))/1000;
				luong*=1000;
				sql = "update luongchitiet set thuclanh="+luong+" where maluongchitiet='"+rs.getString("maluongchitiet")+"'";
				dc.con.createStatement().executeUpdate(sql);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}

	public boolean TinhLuong(int thang) {
		/*
		 * a=(luongcoban+phucap /24/8)
		 * luong = a*sogiolambinhthuong +a*1.5*sogiolamtangca+thuong
		 */
		boolean flag = false;
		dc.Connect();
		int[]soGioLam;
		ChamCongDAO chamCong = new ChamCongDAO();
		String sql = "select * from luongchitiet,chamcong,phucap,thuong,luong where  thang="+thang+" and luong.mabacluong = luongchitiet.mabacluong and thuong.mathuong=luongchitiet.mathuong and phucap.maphucap=luongchitiet.maphucap  and chamcong.machamcong=luongchitiet.machamcong;";
		try {
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			while(rs.next()&&rs!=null)
			{
				flag = true;
				int luong=(int) (rs.getInt("luongcoban")+(rs.getInt("luongcoban")*rs.getDouble("hesophucap")))/24/8;
				soGioLam = chamCong.getSoGioLam(rs.getString("luongchitiet.machamcong"));
				luong = (int) (luong*soGioLam[0]+luong*1.5*soGioLam[1]+rs.getInt("thuong.mucthuong"))/1000;
				luong*=1000;
				sql = "update luongchitiet set thuclanh="+luong+" where maluongchitiet='"+rs.getString("maluongchitiet")+"'";
				dc.con.createStatement().executeUpdate(sql);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
		
	}
	public static void main(String[] args) {
		LuongChiTietDAO l = new  LuongChiTietDAO();
		System.out.println(l.TinhLuongTheoNhanVien(4, "18"));
		System.out.println(l.TinhLuong(5));
		
	}
}
