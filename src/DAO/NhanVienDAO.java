package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.*;

import DTO.NhanVienDTO;

public class NhanVienDAO {
	DataConnection dc = new DataConnection();

	public ArrayList<NhanVienDTO> loadDataByCondition(String cond, String value) {
		ArrayList<NhanVienDTO> list = new ArrayList<>();
		ResultSet result = null;
		String query = "select * from nhanvien";
		if (!cond.isEmpty()) {
			query += " where " + cond + "='" + value + "'";
		}
		try {
			dc.Connect();
			result = dc.con.createStatement().executeQuery(query);
			if (result == null) {
				return null;
			}
			NhanVienDTO staff;
			while (result.next()) {
				staff = new NhanVienDTO(result.getString(1), result.getString(2), result.getString(3),
						result.getString(4), result.getString(6), result.getString(7), result.getString(9),
						result.getString(11), result.getString(5), result.getString(15), result.getString(13),
						result.getString(14), result.getString(16), result.getString(10), result.getString(8),
						result.getString(12));
				list.add(staff);
			}
			dc.Disconnect();
			return list;

		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
	}

	public boolean addStaff(NhanVienDTO staff) {
		dc.Connect();
		StringBuilder query = new StringBuilder("insert into nhanvien values('");
		query.append(staff.getId()).append("','");// 1
		query.append(staff.getFirstName()).append("','");// 2
		query.append(staff.getLastName()).append("','");// 3
		query.append(staff.getSex()).append("','");// 4
		query.append(staff.getBirthday()).append("','");// 5
		query.append(staff.getAddress()).append("','");// 6
		query.append(staff.getNation()).append("','");// 7
		query.append(staff.getIdCard()).append("','");// 8
		query.append(staff.getPhoneNumber()).append("','");// 9
		query.append(staff.getMail()).append("','");// 10
		query.append(staff.getAccount()).append("','");// 11
		query.append(staff.getPathImg()).append("','");// 12
		query.append(staff.getIdContract()).append("','");// 13
		query.append(staff.getIdPosition()).append("','");// 14
		query.append(staff.getIdDepartment()).append("','");// 15
		query.append(staff.getIdLiteracy()).append("')");// 16
		try {
			int result = dc.con.createStatement().executeUpdate(query.toString());
			if (result >= 1) {
				dc.Disconnect();
				return true;
			}
		} catch (SQLException e) {
			return false;
		}
		dc.Disconnect();
		return false;
	}

	public ArrayList<NhanVienDTO> searchStaff(String[] input) {
		String cond = input[0];
		switch (cond) {
		case "1":
			return loadDataByCondition("manv", input[1]);
		case "2":
			return loadDataByCondition("ten", input[1]);
		case "3":
			return loadDataByCondition("maphongban", input[1]);
		case "4":
			return loadDataByCondition("macv", input[1]);
		case "5":
			return loadDataByCondition("email", input[1]);
		default:
			return null;
		}
	}

	@SuppressWarnings("unused")
	public boolean deleteStaff(String id) {
		String query = "delete from nhanvien where manv='" + id + "'";
		dc.Connect();
		try {
			int k1 = dc.con.createStatement().executeUpdate(query);
			dc.Disconnect();
			return true;

		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}

	public NhanVienDTO getStaff(String id) {
		dc.Connect();
		String query = "select * from nhanvien where manv='" + id + "'";
		ResultSet result = null;
		try {
			result = dc.con.createStatement().executeQuery(query);
			NhanVienDTO staff = new NhanVienDTO();
			while (result.next()) {
				staff.setId(result.getString(1));
				staff.setFirstName(result.getString(2));
				staff.setLastName(result.getString(3));
				if (result.getString(4).equals("Nam")) {
					staff.setSex("Nam");
				} else {
					staff.setSex("Nữ");
				}
				staff.setBirthday(result.getString(5));
				staff.setAddress(result.getString(6));
				staff.setNation(result.getString(7));
				staff.setIdCard(result.getString(8));
				staff.setPhoneNumber(result.getString(9));
				staff.setMail(result.getString(10));
				staff.setAccount(result.getString(11));
				staff.setPathImg(result.getString(12));
				staff.setIdContract(result.getString(13));
				staff.setIdPosition(result.getString(14));
				staff.setIdDepartment(result.getString(15));
				staff.setIdLiteracy(result.getString(16));
			}
			dc.Disconnect();
			return staff;
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}

	}

	public int countStaffByCodition(String condition, String value) {
		String query = "Select count(manv) from nhanvien where " + condition + "='" + value + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				int k = result.getInt(1);
				dc.Disconnect();
				return k;
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Đã xảy ra lỗi");
			return -1;
		}
		return 0;
	}

	public String getId(String col, String id, String value) {
		String query = "select " + col + " from nhanvien where " + id + "='" + value + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				return result.getString(1);
			}
		} catch (SQLException e) {
			return "";
		}
		return "";
	}

	public boolean updateOneCol(String col, String newValue, String cond, String value) {// update
																							// 1
																							// cột
		String query = "update quanlynhansu.nhanvien set " + col + "='" + newValue + "' where " + cond + "='" + value
				+ "'";
		dc.Connect();
		try {
			int k = dc.con.createStatement().executeUpdate(query);
			if (k >= 0) {
				dc.Disconnect();
				return true;
			}
			dc.Disconnect();
			return true;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}

	public ArrayList<String> getListOneCol(String col) {
		String query = "select " + col + " from nhanvien";
		ArrayList<String> list = new ArrayList<>();
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				list.add(result.getString(1));
			}
			dc.Disconnect();
			return list;
		} catch (SQLException e) {
			return null;
		}
	}

	public String createNewId() {
		int max = 0;
		ArrayList<String> listId = getListOneCol("manv");
		if (!listId.isEmpty()) {
			for (String st : listId) {
				if (Integer.parseInt(st) > max) {
					max = Integer.parseInt(st);
				}
			}
		}
		max++;
		String id = String.valueOf(max);
		return id;
	}

	// update nhan vien
	public boolean updateStaff(NhanVienDTO staff) {
		StringBuilder query = new StringBuilder();
		query.append("update quanlynhansu.nhanvien set ho='" + staff.getFirstName() + "',");
		query.append(" ten='" + staff.getLastName() + "', gioitinh='" + staff.getSex() + "', ngaysinh='"
				+ staff.getBirthday() + "',");
		query.append(" diachi='" + staff.getAddress() + "', dantoc='" + staff.getNation() + "', socmnd='"
				+ staff.getIdCard() + "',");
		query.append(" sodt='" + staff.getPhoneNumber() + "', email='" + staff.getMail() + "', sotaikhoan='"
				+ staff.getAccount() + "',");
		query.append(" hinhanh='" + staff.getPathImg() + "', mahdld='" + staff.getIdContract() + "', macv='"
				+ staff.getIdPosition() + "',");
		query.append(" maphongban='" + staff.getIdDepartment() + "', matdhv='" + staff.getIdLiteracy() + "'");
		query.append(" where manv='"+staff.getId()+"'");
		dc.Connect();
		try {
			@SuppressWarnings("unused")
			int k = dc.con.createStatement().executeUpdate(query.toString());
			dc.Disconnect();
			return true;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}
}
