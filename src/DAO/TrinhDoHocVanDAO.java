package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import DTO.LiteracyDTO;

public class TrinhDoHocVanDAO {
	DataConnection dc = new DataConnection();

	public String getNameById(String col, String id) {
		String query = "select " + col + " from quanlynhansu.trinhdohocvan where matdhv='" + id + "'";
		dc.Connect();
		ResultSet result = null;
		try {
			result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				String st = result.getString(1);
				dc.Disconnect();

				return st;
			}
		} catch (SQLException e) {
			dc.Disconnect();
			JOptionPane.showMessageDialog(null, "Lấy tên trình độ học vấn lỗi!!!");
			return "";
		}
		return "";
	}

	public ArrayList<String> getListName(String col) {// lấy tên trình độ học
														// vấn hoặc chuyên ngành
		String query = "select distinct " + col + " from quanlynhansu.trinhdohocvan";
		ArrayList<String> list = new ArrayList<>();
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				list.add(result.getString(1));
			}
			dc.Disconnect();
			return list;
		} catch (SQLException e) {
			dc.Disconnect();
			JOptionPane.showMessageDialog(null, "Lỗi lấy danh sách");
			return null;
		}
	}
	//kiểm tra xem có tồn tại trình độ học vấn này chưa, có rồi trả về true, nguoc lại false;
	public boolean check(String name, String valuename, String day, String valueday) {
		String query = "select * from quanlynhansu.trinhdohocvan where " + name + "='" + valuename + "' and " + day
				+ "='" + valueday + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				dc.Disconnect();
				return true;
			}
			dc.Disconnect();
			return false;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}

	public String createNewId() {
		int max = 0;
		ArrayList<String> listId = getListName("matdhv");
		if (!listId.isEmpty()) {
			for (String st : listId) {
				if (Integer.parseInt(st.substring(4)) > max) {
					max = Integer.parseInt(st.substring(4));
				}
			}
		}
		max++;
		String newId = "TDHV" + String.valueOf(max);
		return newId;
	}

	public boolean insert(LiteracyDTO literacy) {
		String query = "insert into quanlynhansu.trinhdohocvan values('" + literacy.getIdLiteracy() + "','"
				+ literacy.getLiteracyName() + "','" + literacy.getSpecialization() + "')";
		dc.Connect();
		try {
			int k=dc.con.createStatement().executeUpdate(query);
			dc.Disconnect();
			return k>=1?true:false;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}
	public String getIdByCondition(String valueName, String specializeName){//lấy id bằng tên tdhv và chuyên ngành
		String query="select matdhv from quanlynhansu.trinhdohocvan where tentdhv='"+valueName+"' and chuyennganh='"+specializeName+"'";
		dc.Connect();
		ResultSet result=null;
		try {
			result=dc.con.createStatement().executeQuery(query);
			if(result.next()){
				String id=result.getString(1);
				dc.Disconnect();
				return id;
			}
			dc.Disconnect();
			return "";
		} catch (SQLException e) {
			dc.Disconnect();
			return "";
		}	
		
	}
}
