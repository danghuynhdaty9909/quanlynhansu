package BUS;

import java.sql.ResultSet;
import java.util.ArrayList;

import DAO.PhongBanDAO;
import DTO.DepartmentDTO;
import DTO.NhanVienDTO;

public class PhongBanBUS {
	PhongBanDAO departmentDao=new PhongBanDAO();
	public String getIdDepartment(String name) {
		return departmentDao.getIdDepartment(name);
	}

	public ArrayList<DepartmentDTO> loadData() {
		return departmentDao.loadData();
	}

	public String getNameBoss(String id) {
		return departmentDao.getNameBoss(id);
	}

	public String getNameDepartment(String id) {
		return departmentDao.getNameDepartment(id);
	}

	public ArrayList<String> getColByCondition(String cond, String value) {
		return departmentDao.getColByCondition(cond, value);
	}

	public boolean update(DepartmentDTO dto) {
		return departmentDao.update(dto);
	}

	public ArrayList<String> getListOneCol(String col, String cond, String value) {
		return departmentDao.getListOneCol(col, cond, value);
	}

	public ArrayList<NhanVienDTO> getList() {
		return departmentDao.getList();
	}

	public boolean insert(DepartmentDTO depart) {
		return  departmentDao.insert(depart);
	}

	public ResultSet countStaff() {
		return departmentDao.countStaff();
	}
	public String getAddress(String id){
		return departmentDao.getAddress(id);
	}
	public String getIdBoss(String idDepart){
		return departmentDao.getIdBoss(idDepart);
	}
}
