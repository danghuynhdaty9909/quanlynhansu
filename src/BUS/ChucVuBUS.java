package BUS;

import java.util.ArrayList;

import DAO.ChucVuDAO;

public class ChucVuBUS {
	ChucVuDAO literacyDao=new ChucVuDAO();
	public String getIdPositionByName(String name){
		return literacyDao.getIdPositinByName(name);
	}
	public String getNameById(String id){
		return literacyDao.getNameById(id);
	}
	public ArrayList<String> getListNameByCond(String col,String cond, String value){
		return literacyDao.getListNameByCond(col, cond, value);
	}
}
