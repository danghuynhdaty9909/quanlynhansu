package BUS;

import java.util.ArrayList;

import DAO.HopDongLaoDongDAO;
import DTO.ContractDTO;

public class HopDongLaoDongBUS {
	HopDongLaoDongDAO contractDao=new HopDongLaoDongDAO();
	public String getIdContractByName(String name){
		return contractDao.getIdContractByName(name);
	}
	public String getName(String id){
		return contractDao.getName(id);
	}
	public boolean delete(String idContract){
		return contractDao.delete(idContract);
	}
	public ArrayList<String> getListName(){
		return contractDao.getListName();
	}
	public ArrayList<String> getList(String col){
		return contractDao.getList(col);
	}
	public boolean check(String name,String valuename, String day,String valueday){
		return contractDao.check(name, valuename, day, valueday);
	}
	public boolean insert(ContractDTO contract){
		return contractDao.insert(contract);
	}
	public String getNewId(){
		return contractDao.createNewId();
	}
	public boolean update(ContractDTO contract){
		return contractDao.update(contract);
	}
}
