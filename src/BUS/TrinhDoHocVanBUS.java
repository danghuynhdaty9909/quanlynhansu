package BUS;

import java.util.ArrayList;

import DAO.TrinhDoHocVanDAO;
import DTO.LiteracyDTO;

public class TrinhDoHocVanBUS {
	TrinhDoHocVanDAO literacyDao=new TrinhDoHocVanDAO();
	public String getNameById(String col,String id){
		return literacyDao.getNameById(col,id);
	}
	public ArrayList<String> getListName(String col){
		return literacyDao.getListName(col);
	}
	public boolean check(String name,String valuename, String day,String valueday){
		return literacyDao.check(name, valuename, day, valueday);
	}
	public String createNewId(){
		return literacyDao.createNewId();
	}
	public boolean insert(LiteracyDTO literacy){
		return literacyDao.insert(literacy);
	}
	public String getIdByCondition(String valueName, String specializeName){
		return literacyDao.getIdByCondition(valueName, specializeName);
	}
}
