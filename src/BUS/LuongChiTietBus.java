package BUS;

import DAO.LuongChiTietDAO;

public class LuongChiTietBus {
	LuongChiTietDAO l =new LuongChiTietDAO();
	
	public boolean TinhLuongTheoNhanVien(int thang, String maNhanVien) {
		return l.TinhLuongTheoNhanVien(thang, maNhanVien);
	}
	
	public boolean TinhLuong(int thang)
	{
		return l.TinhLuong(thang);
	}
}
