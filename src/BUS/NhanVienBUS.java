package BUS;

import java.util.ArrayList;
import DAO.NhanVienDAO;
import DTO.NhanVienDTO;

public class NhanVienBUS {
	NhanVienDAO staffDao = new NhanVienDAO();

	public ArrayList<NhanVienDTO> loadDataByCondition(String cond, String value) {
		return staffDao.loadDataByCondition(cond, value);
	}

	public boolean addStaff(NhanVienDTO staff) {
		return staffDao.addStaff(staff);
	}

	public ArrayList<NhanVienDTO> searchStaff(String[] input) {
		return staffDao.searchStaff(input);
	}

	public boolean deleteStaff(String id) {
		return staffDao.deleteStaff(id);
	}

	public NhanVienDTO getStaff(String id) {
		return staffDao.getStaff(id);
	}

	public int countStaffByCodition(String condition, String value) {
		return staffDao.countStaffByCodition(condition, value);
	}

	public String getId(String col, String id, String value) {
		return staffDao.getId(col, id, value);
	}

	public boolean updateOneCol(String col, String newValue, String cond, String value) {
		return staffDao.updateOneCol(col, newValue, cond, value);
	}

	public ArrayList<String> getListOneCol(String col) {
		return staffDao.getListOneCol(col);
	}

	public String createNewId() {
		return staffDao.createNewId();
	}

	public boolean updateStaff(NhanVienDTO staff) {
		return staffDao.updateStaff(staff);
	}
}
