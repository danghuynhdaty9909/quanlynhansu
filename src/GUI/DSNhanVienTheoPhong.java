package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import DTO.DepartmentDTO;
import DTO.NhanVienDTO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class DSNhanVienTheoPhong extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JTable table;
	private DefaultTableModel model;
	private JButton btOk;
	private JButton btClose;
	private static String newIdBoss=QuanLyPhongBan.getIdOldBoss();
	private static String idPosition="";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DSNhanVienTheoPhong frame = new DSNhanVienTheoPhong();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DSNhanVienTheoPhong() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		

		JPanel panel = new JPanel();
		panel.setBounds(10, 93, 1330, 387);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setFont(new Font("SansSerif", Font.BOLD, 12));
		table.setModel(model=new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"M\u00E3 Nh\u00E2n Vi\u00EAn", "H\u1ECD", "T\u00EAn", "Gi\u1EDBi T\u00EDnh", "Email", "S\u1ED1 \u0110i\u1EC7n Tho\u1EA1i", "S\u1ED1 CMND", "Qu\u00EA Qu\u00E1n", "M\u00E3 ph\u00F2ng ban", "M\u00E3 ch\u1EE9c v\u1EE5", "M\u00E3 h\u1EE3p \u0111\u1ED3ng", "M\u00E3 TDHV"
				}
			));
		scrollPane.setViewportView(table);

		JLabel lblNewLabel = new JLabel("Danh Sách Nhân Viên");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 30));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(89, 11, 1163, 71);
		contentPane.add(lblNewLabel);

		btOk = new JButton("Đồng Ý");
		btOk.setBounds(436, 529, 89, 23);
		contentPane.add(btOk);

		btClose = new JButton("Đóng");
		btClose.setBounds(835, 529, 89, 23);
		contentPane.add(btClose);
		PhongBanBUS bus=new PhongBanBUS();
		String id=bus.getIdDepartment(QuanLyPhongBan.getCurDepart().getName());
		loadData(id);
		btOk.addActionListener(this);
		btClose.addActionListener(this);
	}
	public void loadData(String id){
		NhanVienBUS bus=new NhanVienBUS();
		ArrayList<NhanVienDTO> list=bus.loadDataByCondition("maphongban",id);
		Vector<String> row;
		for(NhanVienDTO staff:list){
			row = new Vector<>();
			row.add(staff.getId());
			row.add(staff.getFirstName());
			row.add(staff.getLastName());
			row.add(staff.getSex());
			row.add(staff.getMail());
			row.add(staff.getPhoneNumber());
			row.add(staff.getIdCard());
			row.add(staff.getAddress());
			row.add(staff.getIdDepartment());
			row.add(staff.getIdPosition());
			row.add(staff.getIdContract());
			row.add(staff.getIdLiteracy());
			model.addRow(row);
		}
		table.setModel(model);
	}
	public void actionPerformed(ActionEvent e){
		DepartmentDTO dto=QuanLyPhongBan.getCurDepart();
		SuaPhongBan fix=new SuaPhongBan();
		fix.getTxtName().setText(dto.getName());
		fix.getTxtAddress().setText(dto.getAddress());
		fix.setVisible(true);
		setVisible(false);
		if(e.getSource()==btOk){
			int k=getTable().getSelectedRow();
			if(k>=0){
				fix.getTxtBoss().setText(getModel().getValueAt(k, 1)+" "+getModel().getValueAt(k, 2));
				newIdBoss=((String) model.getValueAt(k, 0));
				idPosition=(String)model.getValueAt(k, 9);
			}else{
				JOptionPane.showMessageDialog(null, "Xin chọn  dòng chứa thông tin nhân viên làm trưởng phòng!!!");
			}
		}
	}
	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public DefaultTableModel getModel() {
		return model;
	}

	public void setModel(DefaultTableModel model) {
		this.model = model;
	}

	public static String getNewIdBoss() {
		return newIdBoss;
	}

	public static void setNewIdBoss(String newIdBoss) {
		DSNhanVienTheoPhong.newIdBoss = newIdBoss;
	}

	public static String getIdPosition() {
		return idPosition;
	}

	public static void setIdPosition(String idPosition) {
		DSNhanVienTheoPhong.idPosition = idPosition;
	}
	
	
}
