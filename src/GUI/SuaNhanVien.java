package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.ChucVuBUS;
import BUS.HopDongLaoDongBUS;
import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import BUS.TrinhDoHocVanBUS;
import DTO.NhanVienDTO;

import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.SwingConstants;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;

@SuppressWarnings("serial")
public class SuaNhanVien extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField txtSearchInput;
	private JButton btFix;
	private JLabel btReturn;
	private DefaultTableModel model;
	@SuppressWarnings("rawtypes")
	private JComboBox cbSearchInput;
	NhanVienBUS staffBus = new NhanVienBUS();
	private static NhanVienDTO oldStaff=new NhanVienDTO();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SuaNhanVien frame = new SuaNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public SuaNhanVien() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\sua.png"));
		setTitle("Sửa Nhân Viên");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 364, 1350, 343);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setModel(model = new DefaultTableModel(new Object[][] {},
				new String[] { "M\u00E3 Nh\u00E2n Vi\u00EAn", "H\u1ECD", "T\u00EAn", "Gi\u1EDBi T\u00EDnh", "Email",
						"S\u1ED1 \u0110i\u1EC7n Tho\u1EA1i", "S\u1ED1 CMND", "\u0110\u1ECBa Ch\u1EC9",
						"M\u00E3 Ph\u00F2ng Ban", "M\u00E3 Ch\u1EE9c V\u1EE5", "M\u00E3 HDLD", "M\u00E3 TDHV" }));
		table.getColumnModel().getColumn(1).setPreferredWidth(82);
		table.getColumnModel().getColumn(2).setPreferredWidth(59);
		table.getColumnModel().getColumn(3).setPreferredWidth(52);
		table.getColumnModel().getColumn(4).setPreferredWidth(105);
		table.getColumnModel().getColumn(6).setPreferredWidth(69);
		table.getColumnModel().getColumn(7).setPreferredWidth(121);
		table.getColumnModel().getColumn(8).setPreferredWidth(78);
		table.getColumnModel().getColumn(10).setPreferredWidth(55);
		table.getColumnModel().getColumn(11).setPreferredWidth(58);
		table.setRowHeight(25);
		scrollPane.setViewportView(table);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(100, 0, 1150, 365);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		JLabel lbSearchInput = new JLabel("TÌM KIẾM NHÂN SỰ:");
		lbSearchInput.setFont(new Font("Tahoma", Font.BOLD, 12));
		lbSearchInput.setBounds(190, 160, 132, 25);
		panel_1.add(lbSearchInput);

		txtSearchInput = new JTextField();
		txtSearchInput.setBounds(332, 160, 242, 25);
		panel_1.add(txtSearchInput);
		txtSearchInput.setColumns(10);

		cbSearchInput = new JComboBox();
		cbSearchInput.setFont(new Font("Tahoma", Font.BOLD, 12));
		cbSearchInput
				.setModel(new DefaultComboBoxModel(new String[] { "Tìm kiếm theo Email", "Tìm kiếm theo Mã Chức Vụ",
						"Tìm kiếm theo Mã Phòng Ban", "Tìm kiếm theo Tên Nhân Viên", "Tìm kiếm theo Mã Nhân Viên" }));
		cbSearchInput.setSelectedIndex(4);
		cbSearchInput.setBounds(596, 160, 233, 21);
		panel_1.add(cbSearchInput);

		JLabel lbSearch = new JLabel("");
		lbSearch.setIcon(new ImageIcon("img\\search.png"));
		lbSearch.setBounds(839, 149, 48, 42);
		lbSearch.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {
				lbSearch.setBorder(null);

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lbSearch.setBorder(new LineBorder(Color.GRAY));

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				String[] input = new String[2];
				input[1] = txtSearchInput.getText();
				String cond = (String) cbSearchInput.getSelectedItem();
				switch (cond) {
				case "Tìm kiếm theo Mã Nhân Viên":
					input[0] = "1";
					break;
				case "Tìm kiếm theo Tên Nhân Viên":
					input[0] = "2";
					break;
				case "Tìm kiếm theo Mã Phòng Ban":
					input[0] = "3";
					break;
				case "Tìm kiếm theo Mã Chức Vụ":
					input[0] = "4";
					break;
				case "Tìm kiếm theo Email":
					input[0] = "5";
					break;
				}
				NhanVienBUS staffBus = new NhanVienBUS();
				ArrayList<NhanVienDTO> list = staffBus.searchStaff(input);
				if (list.isEmpty()) {
					JOptionPane.showMessageDialog(null, "Không tìm thấy nhân viên!!");
				} else {
					Vector<String> row;
					for (NhanVienDTO staff : list) {
						row = new Vector<>();
						row.add(staff.getId());
						row.add(staff.getFirstName());
						row.add(staff.getLastName());
						row.add(staff.getSex());
						row.add(staff.getMail());
						row.add(staff.getPhoneNumber());
						row.add(staff.getIdCard());
						row.add(staff.getAddress());
						row.add(staff.getIdDepartment());
						row.add(staff.getIdPosition());
						row.add(staff.getIdContract());
						row.add(staff.getIdLiteracy());
						model.addRow(row);
					}
				}
				table.setModel(model);

			}
		});
		panel_1.add(lbSearch);

		btFix = new JButton("SỬA");
		btFix.setIcon(new ImageIcon("img\\sua.png"));
		btFix.setFont(new Font("Tahoma", Font.BOLD, 11));
		btFix.setBounds(462, 243, 151, 60);
		panel_1.add(btFix);

		JLabel lblNewLabel_2 = new JLabel("Sửa Nhân Viên");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setForeground(Color.CYAN);
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 30));
		lblNewLabel_2.setBounds(369, 43, 344, 53);
		panel_1.add(lblNewLabel_2);

		btReturn = new JLabel("");
		btReturn.setIcon(new ImageIcon("img\\return.png"));
		btReturn.setBounds(1045, 11, 84, 65);
		panel_1.add(btReturn);

		clickFix();

		clickReturn();

	}

	public void clickFix() {
		btFix.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int k = table.getSelectedRow();
				XuatSuaNV fixStaff = new XuatSuaNV();
				if (k >= 0) {
					oldStaff = staffBus.getStaff((String) model.getValueAt(k, 0));
					if (oldStaff != null) {
						fixStaff.getTxtFirstName().setText(oldStaff.getFirstName());
						fixStaff.getTxtLastName().setText(oldStaff.getLastName());
						if ("Nữ".equals(model.getValueAt(k, 3))) {
							fixStaff.getRbtFemale().setSelected(true);
							fixStaff.getRbtMale().setSelected(false);
						} else {
							fixStaff.getRbtMale().setSelected(true);
							fixStaff.getRbtFemale().setSelected(false);
						}
						fixStaff.getTxtEmail().setText(oldStaff.getMail());
						fixStaff.getTxtAddress().setText(oldStaff.getAddress());
						fixStaff.getTxtId().setText(oldStaff.getId());
						fixStaff.getTxtId().setEnabled(false);
						fixStaff.getTxtPhoneNumber().setText(oldStaff.getPhoneNumber());
						fixStaff.getTxtAccount().setText(oldStaff.getAccount());
						fixStaff.getTxtIdCard().setText(oldStaff.getIdCard());
						String birthday = oldStaff.getBirthday();
						fixStaff.getTxtNation().setText(oldStaff.getNation());
						String year = birthday.substring(0, 4);
						String day = birthday.substring(8);
						String month = birthday.substring(5, 7);
						fixStaff.getCbYear().setSelectedItem(year);
						fixStaff.getCbDay().setSelectedItem(day);
						fixStaff.getCbMonth().setSelectedItem(month);
						PhongBanBUS busDepart = new PhongBanBUS();
						ChucVuBUS busPosition = new ChucVuBUS();
						HopDongLaoDongBUS busContract = new HopDongLaoDongBUS();
						fixStaff.getCbDepartmentName()
								.setSelectedItem(busDepart.getNameDepartment((String) model.getValueAt(k, 8)));
						fixStaff.getCbPositionName()
								.setSelectedItem(busPosition.getNameById((String) model.getValueAt(k, 9)));
						fixStaff.getCbContractName()
								.setSelectedItem(busContract.getName((String) model.getValueAt(k, 10)));
						fixStaff.getTxtAccount().setText(oldStaff.getAccount());
						fixStaff.getLbImg().setIcon(new ImageIcon("img\\imgnhansu\\"+oldStaff.getPathImg()));
						fixStaff.setVisible(true);
						setVisible(false);
						}
				}else{
					JOptionPane.showMessageDialog(null, "Vui lòng chọn dòng chứa thông tin nhân viên cần sửa rồi bấm sửa!!!");
				}

			}
		});
	}
	
	public void clickReturn() {
		btReturn.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new QuanLyNhanSu().setVisible(true);

			}
		});
	}

	public static NhanVienDTO getOldStaff() {
		return oldStaff;
	}

	public static void setOldStaff(NhanVienDTO oldStaff) {
		SuaNhanVien.oldStaff = oldStaff;
	}
	
}
