package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import BUS.NhanVienBUS;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

public class XuatTimKiem extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtMail;
	private JTextField txtId;
	private JTextField txtPhoneNumber;
	private JTextField txtIdCard;
	private JTextField txtLiteracyName;
	private JTextField txtContractName;
	private JTable table;
	private JButton btDong;
	private JButton btClose;
	private JTextField txtNation;
	private JTextField txtAddress;
	private JTextField txtSpecialized;
	private JTextField txtDepartmentName;
	private JTextField txtPositionName;
	private JTextField txtBirthday;
	private JTextField txtAccount;
	private JRadioButton rbtMale;
	private JRadioButton rbtFemale;
	private JLabel lbImg;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					XuatTimKiem frame = new XuatTimKiem();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public XuatTimKiem() {
		setTitle("THÔNG TIN NHÂN SỰ");
		setIconImage(
				Toolkit.getDefaultToolkit().getImage("img\\officer.png"));
		setBounds(100, 100, 1366, 768);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setForeground(Color.BLACK);
		panel.setBounds(159, 50, 1055, 729);
		panel.setLayout(null);

		JLabel lb0 = new JLabel("Thông Tin Nhân Viên Cần Tìm");
		lb0.setForeground(Color.BLUE);
		lb0.setFont(new Font("SansSerif", Font.BOLD, 30));
		lb0.setBounds(349, 72, 457, 57);
		panel.add(lb0);

		JLabel lbFirstName = new JLabel("Họ:");
		lbFirstName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbFirstName.setBounds(200, 197, 30, 20);
		panel.add(lbFirstName);

		txtFirstName = new JTextField();
		txtFirstName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtFirstName.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		txtFirstName.setBounds(251, 197, 126, 23);
		panel.add(txtFirstName);
		txtFirstName.setColumns(10);

		JLabel lnLastName = new JLabel("Tên:");
		lnLastName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lnLastName.setBounds(387, 197, 46, 20);
		panel.add(lnLastName);

		txtLastName = new JTextField();
		txtLastName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtLastName.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		txtLastName.setBounds(443, 197, 121, 23);
		panel.add(txtLastName);
		txtLastName.setColumns(10);

		ButtonGroup bg = new ButtonGroup();

		rbtMale = new JRadioButton("Nam");
		bg.add(rbtMale);
		rbtMale.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rbtMale.setBounds(743, 197, 52, 23);
		panel.add(rbtMale);

		rbtFemale = new JRadioButton("Nữ");
		bg.add(rbtFemale);
		rbtFemale.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rbtFemale.setBounds(814, 197, 41, 23);
		panel.add(rbtFemale);

		JLabel lbId = new JLabel("Mã Nhân Viên:");
		lbId.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbId.setBounds(200, 275, 112, 20);
		panel.add(lbId);

		txtMail = new JTextField();
		txtMail.setBorder(new BevelBorder(BevelBorder.RAISED, new Color(192, 192, 192), Color.WHITE, null, null));
		txtMail.setFont(new Font("SansSerif", Font.PLAIN, 12));
		txtMail.setBounds(349, 236, 215, 20);
		panel.add(txtMail);
		txtMail.setColumns(10);

		JLabel lbNation = new JLabel("Dân tộc:");
		lbNation.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbNation.setBounds(635, 236, 61, 20);
		panel.add(lbNation);


		JLabel lbMail = new JLabel("Email:");
		lbMail.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbMail.setBounds(200, 236, 46, 20);
		panel.add(lbMail);

		JLabel lbPhoneNumber = new JLabel("Số Điện Thoại:");
		lbPhoneNumber.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbPhoneNumber.setBounds(200, 314, 105, 20);
		panel.add(lbPhoneNumber);

		JLabel lbIdCard = new JLabel("Số CMND:");
		lbIdCard.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbIdCard.setBounds(200, 393, 78, 20);
		panel.add(lbIdCard);

		JLabel lbDepartment = new JLabel("Tên Phòng Ban:");
		lbDepartment.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbDepartment.setBounds(200, 432, 121, 20);
		panel.add(lbDepartment);

		JLabel lbPositionName = new JLabel("Tên Chức Vụ: ");
		lbPositionName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbPositionName.setBounds(200, 471, 105, 20);
		panel.add(lbPositionName);

		JLabel lbContractName = new JLabel("Tên HDLD:");
		lbContractName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbContractName.setBounds(635, 314, 78, 20);
		panel.add(lbContractName);

		JLabel lbLiteracyName = new JLabel("Tên TDHV:");
		lbLiteracyName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbLiteracyName.setBounds(635, 354, 105, 20);
		panel.add(lbLiteracyName);

		txtId = new JTextField();
		txtId.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtId.setBounds(349, 275, 215, 20);
		panel.add(txtId);
		txtId.setColumns(10);

		txtPhoneNumber = new JTextField();
		txtPhoneNumber.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtPhoneNumber.setBounds(349, 314, 215, 20);
		panel.add(txtPhoneNumber);
		txtPhoneNumber.setColumns(10);

		txtIdCard = new JTextField();
		txtIdCard.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtIdCard.setBounds(349, 393, 215, 20);
		panel.add(txtIdCard);
		txtIdCard.setColumns(10);

		txtContractName = new JTextField();
		txtContractName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtContractName.setBounds(743, 314, 215, 20);
		panel.add(txtContractName);
		txtContractName.setColumns(10);

		txtLiteracyName = new JTextField();
		txtLiteracyName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtLiteracyName.setBounds(743, 354, 215, 20);
		panel.add(txtLiteracyName);
		txtLiteracyName.setColumns(10);

		JLabel lbAddress = new JLabel("Địa chỉ:");
		lbAddress.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbAddress.setBounds(635, 275, 78, 20);
		panel.add(lbAddress);


		JLabel lbSex = new JLabel("Giới Tính:");
		lbSex.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbSex.setBounds(635, 197, 82, 20);
		panel.add(lbSex);

		table = new JTable();
		table.setBounds(1340, 699, -1043, -138);
		panel.add(table);

		getContentPane().add(panel);
		
		lbImg = new JLabel("");
		lbImg.setBounds(200, 55, 121, 123);
		panel.add(lbImg);
		
		btClose = new JButton("Đóng");
		btClose.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btClose.setBounds(513, 532, 97, 49);
		btClose.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new TimKiem().setVisible(true);
				
			}
		});
		panel.add(btClose);
		
		txtNation = new JTextField();
		txtNation.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtNation.setBounds(743, 237, 215, 20);
		panel.add(txtNation);
		txtNation.setColumns(10);
		
		txtAddress = new JTextField();
		txtAddress.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtAddress.setBounds(743, 277, 215, 20);
		panel.add(txtAddress);
		txtAddress.setColumns(10);
		
		JLabel lbBirthday = new JLabel("Ngày Sinh:");
		lbBirthday.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbBirthday.setBounds(200, 354, 78, 23);
		panel.add(lbBirthday);
		
		txtSpecialized = new JTextField();
		txtSpecialized.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtSpecialized.setBounds(743, 393, 215, 20);
		panel.add(txtSpecialized);
		txtSpecialized.setColumns(10);
		
		JLabel lbSpecialized = new JLabel("Chuyên ngành:");
		lbSpecialized.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbSpecialized.setBounds(635, 393, 112, 20);
		panel.add(lbSpecialized);
		
		txtDepartmentName = new JTextField();
		txtDepartmentName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtDepartmentName.setBounds(349, 432, 215, 20);
		panel.add(txtDepartmentName);
		txtDepartmentName.setColumns(10);
		
		txtPositionName = new JTextField();
		txtPositionName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtPositionName.setBounds(349, 471, 215, 20);
		panel.add(txtPositionName);
		txtPositionName.setColumns(10);
		
		txtBirthday = new JTextField();
		txtBirthday.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtBirthday.setBounds(349, 354, 215, 20);
		panel.add(txtBirthday);
		txtBirthday.setColumns(10);
		
		JLabel lbAccount = new JLabel("Số tài khoản:");
		lbAccount.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbAccount.setBounds(635, 432, 105, 20);
		panel.add(lbAccount);
		
		txtAccount = new JTextField();
		txtAccount.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtAccount.setBounds(743, 432, 215, 20);
		panel.add(txtAccount);
		txtAccount.setColumns(10);
		
		

	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JTextField getTxtFirstName() {
		return txtFirstName;
	}

	public void setTxtFirstName(JTextField txtFirstName) {
		this.txtFirstName = txtFirstName;
	}

	public JTextField getTxtLastName() {
		return txtLastName;
	}

	public void setTxtLastName(JTextField txtLastName) {
		this.txtLastName = txtLastName;
	}

	public JTextField getTxtMail() {
		return txtMail;
	}

	public void setTxtMail(JTextField txtMail) {
		this.txtMail = txtMail;
	}

	public JTextField getTxtId() {
		return txtId;
	}

	public void setTxtId(JTextField txtId) {
		this.txtId = txtId;
	}

	public JTextField getTxtPhoneNumber() {
		return txtPhoneNumber;
	}

	public void setTxtPhoneNumber(JTextField txtPhoneNumber) {
		this.txtPhoneNumber = txtPhoneNumber;
	}

	public JTextField getTxtIdCard() {
		return txtIdCard;
	}

	public void setTxtIdCard(JTextField txtIdCard) {
		this.txtIdCard = txtIdCard;
	}

	public JTextField getTxtLiteracyName() {
		return txtLiteracyName;
	}

	public void setTxtLiteracyName(JTextField txtLiteracyName) {
		this.txtLiteracyName = txtLiteracyName;
	}

	public JTextField getTxtContractName() {
		return txtContractName;
	}

	public void setTxtContractName(JTextField txtContractName) {
		this.txtContractName = txtContractName;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JButton getBtDong() {
		return btDong;
	}

	public void setBtDong(JButton btDong) {
		this.btDong = btDong;
	}

	public JButton getBtClose() {
		return btClose;
	}

	public void setBtClose(JButton btClose) {
		this.btClose = btClose;
	}

	public JTextField getTxtNation() {
		return txtNation;
	}

	public void setTxtNation(JTextField txtNation) {
		this.txtNation = txtNation;
	}

	public JTextField getTxtAddress() {
		return txtAddress;
	}

	public void setTxtAddress(JTextField txtAddress) {
		this.txtAddress = txtAddress;
	}

	public JTextField getTextField() {
		return txtSpecialized;
	}

	public void setTextField(JTextField textField) {
		this.txtSpecialized = textField;
	}

	public JTextField getTxtDepartmentName() {
		return txtDepartmentName;
	}

	public void setTxtDepartmentName(JTextField txtDepartmentName) {
		this.txtDepartmentName = txtDepartmentName;
	}

	public JTextField getTxtPositionName() {
		return txtPositionName;
	}

	public void setTxtPositionName(JTextField txtPositionName) {
		this.txtPositionName = txtPositionName;
	}

	public JTextField getTxtBirthday() {
		return txtBirthday;
	}

	public void setTxtBirthday(JTextField txtBirthday) {
		this.txtBirthday = txtBirthday;
	}

	public JTextField getTxtAccount() {
		return txtAccount;
	}

	public void setTxtAccount(JTextField txtAccount) {
		this.txtAccount = txtAccount;
	}

	public JRadioButton getRbtMale() {
		return rbtMale;
	}

	public void setRbtMale(JRadioButton rbtMale) {
		this.rbtMale = rbtMale;
	}

	public JRadioButton getRbtFemale() {
		return rbtFemale;
	}

	public void setRbtFemale(JRadioButton rbtFemale) {
		this.rbtFemale = rbtFemale;
	}

	public JTextField getTxtSpecialized() {
		return txtSpecialized;
	}

	public void setTxtSpecialized(JTextField txtSpecialized) {
		this.txtSpecialized = txtSpecialized;
	}

	public JLabel getLbImg() {
		return lbImg;
	}

	public void setLbImg(JLabel lbImg) {
		this.lbImg = lbImg;
	}
	
}
