package GUI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;

@SuppressWarnings("serial")
public class MainFrame extends JFrame implements ActionListener,MouseListener {

	@SuppressWarnings("unused")
	private JPanel contentPane;
	@SuppressWarnings("unused")
	private JFrame frameChinh;
	private JLabel lbIQLNS;
	private JLabel lbIQLPB;
	private JLabel lbQLNS;
	private JLabel lbQLPB;
	private JLabel LBIThongKe;
	private JLabel lbQLLuong;
	private JLabel lbIQLLuong;
	private JLabel lbThongKe;
	private JLabel lbIDSNS;
	private JLabel lbDSNS;
	private JMenuBar menuBar;
	private JMenu mnFile;
	private JMenuItem mntmExit;
	private JMenuItem mntmQunNhnS;
	private JMenuItem mntmQunLPhng;
	private JMenuItem mntmBoCoThng;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setIconImage(
				Toolkit.getDefaultToolkit().getImage("img\\officer.png"));
		setTitle("Quản Lý Nhân Sự");
		setBounds(100, 100, 1366, 768);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		JLabel lbChinh = new JLabel("Chương Trình Quản Lý Nhân Sự");
		lbChinh.setBounds(0, 2, 1340, 136);
		lbChinh.setHorizontalAlignment(SwingConstants.CENTER);
		lbChinh.setForeground(Color.BLACK);
		lbChinh.setFont(new Font("Times New Roman", Font.BOLD, 25));
		getContentPane().add(lbChinh);

		lbIQLNS = new JLabel("");
		lbIQLNS.setBounds(235, 131, 192, 149);
		lbIQLNS.setHorizontalAlignment(SwingConstants.CENTER);
		lbIQLNS.setIcon(new ImageIcon("img\\QLNS.png"));
		getContentPane().add(lbIQLNS);

		lbIQLPB = new JLabel("");
		lbIQLPB.setBounds(893, 131, 230, 149);
		lbIQLPB.setHorizontalAlignment(SwingConstants.CENTER);
		lbIQLPB.setIcon(new ImageIcon("img\\QTHT.png"));
		getContentPane().add(lbIQLPB);

		lbQLNS = new JLabel("Quản Lý Nhân Sự");
		lbQLNS.setBounds(245, 286, 182, 43);
		lbQLNS.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbQLNS.setHorizontalAlignment(SwingConstants.CENTER);
		lbQLNS.setVerticalAlignment(SwingConstants.TOP);
		getContentPane().add(lbQLNS);

		lbQLPB = new JLabel("Quản Lý Phòng Ban");
		lbQLPB.setBounds(893, 286, 230, 43);
		lbQLPB.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbQLPB.setHorizontalAlignment(SwingConstants.CENTER);
		lbQLPB.setVerticalAlignment(SwingConstants.TOP);
		getContentPane().add(lbQLPB);

		lbIQLLuong = new JLabel("");
		lbIQLLuong.setBounds(235, 415, 192, 149);
		lbIQLLuong.setHorizontalAlignment(SwingConstants.CENTER);
		lbIQLLuong.setIcon(new ImageIcon("img\\BCTK.png"));
		getContentPane().add(lbIQLLuong);

		LBIThongKe = new JLabel("");
		LBIThongKe.setBounds(893, 404, 230, 160);
		LBIThongKe.setHorizontalAlignment(SwingConstants.CENTER);
		LBIThongKe.setIcon(new ImageIcon("img\\iconThongKe.png"));
		getContentPane().add(LBIThongKe);

		lbQLLuong = new JLabel("Quản Lý Lương");
		lbQLLuong.setBounds(235, 570, 192, 83);
		lbQLLuong.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbQLLuong.setVerticalAlignment(SwingConstants.TOP);
		lbQLLuong.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lbQLLuong);

		lbThongKe = new JLabel("Thống Kê");
		lbThongKe.setForeground(Color.BLACK);
		lbThongKe.setBounds(893, 570, 230, 83);
		lbThongKe.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbThongKe.setVerticalAlignment(SwingConstants.TOP);
		lbThongKe.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lbThongKe);
		
		lbIDSNS = new JLabel("");
		lbIDSNS.setHorizontalAlignment(SwingConstants.CENTER);
		lbIDSNS.setIcon(new ImageIcon("img\\danhsach.png"));
		lbIDSNS.setBounds(618, 286, 153, 155);
		getContentPane().add(lbIDSNS);
		
		lbDSNS = new JLabel("Danh Sách Nhân Viên");
		lbDSNS.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbDSNS.setHorizontalAlignment(SwingConstants.CENTER);
		lbDSNS.setBounds(618, 452, 173, 30);
		getContentPane().add(lbDSNS);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnFile = new JMenu("File");
		mnFile.setMnemonic('F');
		menuBar.add(mnFile);
		
		mntmQunLPhng = new JMenuItem("Quản Lý Phòng Ban");
		mntmQunLPhng.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				new QuanLyPhongBan().setVisible(true);
				
			}
		});
		mnFile.add(mntmQunLPhng);
		
		mntmQunNhnS = new JMenuItem("Quản Lý Nhân Sự");
		mntmQunNhnS.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new QuanLyNhanSu().setVisible(true);
				
			}
		});
		mnFile.add(mntmQunNhnS);
		
		mntmBoCoThng = new JMenuItem("Báo Cáo Thống Kê");
		mntmBoCoThng.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new ThongKeNhanVien().setVisible(true);
			}
		});
		mnFile.add(mntmBoCoThng);
		
		mntmExit = new JMenuItem("Exit");
		mntmExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
		mntmExit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
				
			}
		});
		mnFile.add(mntmExit);
		
		clickQLNS();
		clickIQLNS();
		clickQLPB();
		clickIQLPB();
		clickIBCTK();
		clickBCTK();
		clickThongKe();
		clickIThongKe();
		clickIDSNS();
		clickDSNS();
	}
	public void clickIQLNS(){
		lbIQLNS.addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				setVisible(false);
				QuanLyNhanSu ns=new QuanLyNhanSu();
				ns.setVisible(true);
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public void clickQLNS(){
		lbQLNS.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				setVisible(false);
				QuanLyNhanSu ns=new QuanLyNhanSu();
				ns.setVisible(true);
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lbQLNS.setForeground(Color.BLACK);
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lbQLNS.setForeground(Color.CYAN);
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public void clickIQLPB(){
		lbIQLPB.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new QuanLyPhongBan().setVisible(true);
				
			}
		});
	}
	
	public void clickQLPB(){
		lbQLPB.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				lbQLPB.setForeground(Color.RED);
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lbQLPB.setForeground(Color.BLACK);
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lbQLPB.setForeground(Color.CYAN);
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new QuanLyPhongBan().setVisible(true);
				
			}
		});
	}
	
	public void clickIBCTK(){
		lbIQLLuong.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new Luong().setVisible(true);
				
			}
		});
	}
	public void clickBCTK(){
		lbQLLuong.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lbQLLuong.setForeground(Color.BLACK);
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lbQLLuong.setForeground(Color.CYAN);
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new Luong().setVisible(true);
				
			}
		});
	}
	
	public void clickIThongKe(){
		LBIThongKe.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new ThongKeNhanVien().setVisible(true);
				
			}
		});
	}
	public void clickThongKe(){
		lbThongKe.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lbThongKe.setForeground(Color.BLACK);
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lbThongKe.setForeground(Color.CYAN);
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new ThongKeNhanVien().setVisible(true);
				
			}
		});
	}
	
	public void clickIDSNS(){
		lbIDSNS.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new XuatDSNS().setVisible(true);
			}
		});
	}
	
	public void clickDSNS(){
		lbDSNS.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lbDSNS.setForeground(Color.BLACK);
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lbDSNS.setForeground(Color.CYAN);
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new XuatDSNS().setVisible(true);
				
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
			
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
