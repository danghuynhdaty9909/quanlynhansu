package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import BUS.NhanVienBUS;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class TKGioiTinh extends JFrame {

	private JPanel contentPane;
	private int male;
	private int female;
	private JProgressBar jbMale;
	private JProgressBar jbFemale;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TKGioiTinh frame = new TKGioiTinh();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TKGioiTinh() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\sex.jpeg"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);
		
		NhanVienBUS staffBus=new NhanVienBUS();
		male=staffBus.countStaffByCodition("gioitinh", "Nam");
		female=staffBus.countStaffByCodition("gioitinh", "Nữ");
		int tong=male+female;
		int percentNam=male*100/tong;
		int percentNu=female*100/tong;
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(123, 89, 1108, 530);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Thống Kê Giới Tính");
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 25));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(0, 32, 1108, 76);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel("Nam:");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setBounds(190, 187, 65, 22);
		panel.add(lblNewLabel);

		JLabel lblN = new JLabel("Nữ:");
		lblN.setForeground(Color.MAGENTA);
		lblN.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblN.setBounds(190, 338, 65, 22);
		panel.add(lblN);
		
		jbMale = new JProgressBar(0,100);
		jbMale.setForeground(Color.BLACK);
		jbMale.setValue(percentNam);
		jbMale.setStringPainted(true);
		jbMale.setBounds(323, 170, 396, 55);
		panel.add(jbMale);
		
		jbFemale = new JProgressBar(0, 100);
		jbFemale.setForeground(Color.BLACK);
		jbFemale.setValue(percentNu);
		jbFemale.setStringPainted(true);
		jbFemale.setBounds(323, 321, 396, 55);
		panel.add(jbFemale);
		
		JLabel lblNewLabel_2 = new JLabel("Số Lượng:");
		lblNewLabel_2.setFont(new Font("SansSerif", Font.BOLD | Font.ITALIC, 14));
		lblNewLabel_2.setBounds(776, 187, 78, 22);
		panel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Số Lượng:");
		lblNewLabel_3.setFont(new Font("SansSerif", Font.BOLD | Font.ITALIC, 14));
		lblNewLabel_3.setBounds(776, 338, 78, 22);
		panel.add(lblNewLabel_3);
		
		JLabel lbSLNam = new JLabel("");
		lbSLNam.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lbSLNam.setBounds(864, 187, 65, 22);
		String s1=String.valueOf(male);
		lbSLNam.setText(s1);
		panel.add(lbSLNam);
		
		JLabel lbSLNu = new JLabel("");
		lbSLNu.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lbSLNu.setBounds(864, 338, 65, 22);
		String s2=String.valueOf(female);
		lbSLNu.setText(s2);
		panel.add(lbSLNu);
		
		JButton btHuy = new JButton("Đóng");
		btHuy.setForeground(Color.RED);
		btHuy.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btHuy.setBounds(496, 448, 106, 33);
		btHuy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new ThongKeNhanVien().setVisible(true);
				
			}
		});
		panel.add(btHuy);
	}
}
