package GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import DTO.DepartmentDTO;

import java.awt.Color;

public class QuanLyPhongBan extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTable table;
	private JButton btAdd;
	private JButton btFix;
	private JButton btClose;
	private DefaultTableModel model;
	private static DepartmentDTO curDepart = new DepartmentDTO();
	private static String idOldBoss = "";
	private static String oldName = "";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					QuanLyPhongBan frame = new QuanLyPhongBan();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public QuanLyPhongBan() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lbTitle = new JLabel("Danh Sách Phòng Ban");
		lbTitle.setForeground(Color.RED);
		lbTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbTitle.setFont(new Font("Times New Roman", Font.BOLD, 25));
		lbTitle.setBounds(0, 37, 1350, 80);
		contentPane.add(lbTitle);

		btAdd = new JButton("Thêm Mới");
		btAdd.setForeground(Color.BLACK);
		btAdd.setBackground(Color.WHITE);
		btAdd.setFont(new Font("Tahoma", Font.BOLD, 15));
		btAdd.setBounds(96, 632, 118, 42);
		btAdd.addActionListener(this);
		contentPane.add(btAdd);

		table = new JTable();
		table.setFont(new Font("Tahoma", Font.PLAIN, 15));
		table.setModel(model = new DefaultTableModel(new Object[][] {}, new String[] { "T\u00EAn Ph\u00F2ng",
				"Tr\u01B0\u1EDFng Ph\u00F2ng", "\u0110\u1ECBa Ch\u1EC9 Ph\u00F2ng", "S\u1ED1 Nh\u00E2n Vi\u00EAn" }));
		table.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 17));
		table.setRowHeight(25);

		JScrollPane sc = new JScrollPane(table);
		sc.setBounds(96, 146, 1139, 442);
		contentPane.add(sc);

		btFix = new JButton("Sửa");
		btFix.setBackground(Color.WHITE);
		btFix.setFont(new Font("Tahoma", Font.BOLD, 15));
		btFix.setBounds(583, 632, 118, 42);
		btFix.addActionListener(this);
		contentPane.add(btFix);

		btClose = new JButton("Đóng");
		btClose.setBackground(Color.WHITE);
		btClose.setFont(new Font("Tahoma", Font.BOLD, 15));
		btClose.setBounds(1117, 632, 118, 42);
		btClose.addActionListener(this);
		loadData();
		btAdd.addActionListener(this);
		contentPane.add(btClose);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btAdd) {
			 ThemPhongBan addDepart=new ThemPhongBan();
			 addDepart.setVisible(true);
			 setVisible(false);
		}
		if (e.getSource() == btFix) {
			SuaPhongBan fix = new SuaPhongBan();
			int k = table.getSelectedRow();
			if (k >= 0) {
				fix.getTxtName().setText((String) model.getValueAt(k, 0));
				fix.getTxtBoss().setText((String) model.getValueAt(k, 1));
				fix.getTxtAddress().setText((String) model.getValueAt(k, 2));
				PhongBanBUS bus = new PhongBanBUS();
				ArrayList<String> list=bus.getColByCondition("tenphong",(String)model.getValueAt(k, 0));
				idOldBoss=list.get(2);
				curDepart.setId(list.get(0));
				curDepart.setIdBoss(idOldBoss);
				curDepart.setAddress(list.get(3));
				curDepart.setName((String)model.getValueAt(k, 0));
				setVisible(false);
				fix.setVisible(true);
			} else {
				JOptionPane.showMessageDialog(null, "Vui lòng chọn dòng cần sửa!!!");
			}

		}
		if (e.getSource() == btClose) {
			setVisible(false);
			new MainFrame().setVisible(true);
		}

	}
	public void loadData() {

		PhongBanBUS departmentBus = new PhongBanBUS();
		NhanVienBUS staffBus=new NhanVienBUS();
		ArrayList<DepartmentDTO> result = departmentBus.loadData();
		if (result != null) {
			Vector<String> row;
			for (DepartmentDTO department : result) {
				row = new Vector<>();
				row.add(department.getName());
				row.add(departmentBus.getNameBoss(department.getIdBoss()));
				row.add(department.getAddress());
				row.add(String.valueOf( staffBus.countStaffByCodition("maphongban", department.getId())));
				model.addRow(row);

			}
			table.setModel(model);
		}
	}

	public static DepartmentDTO getCurDepart() {
		return curDepart;
	}

	public static void setCurDepart(DepartmentDTO curDepart) {
		QuanLyPhongBan.curDepart = curDepart;
	}

	public static String getIdOldBoss() {
		return idOldBoss;
	}

	public static void setIdOldBoss(String idOldBoss) {
		QuanLyPhongBan.idOldBoss = idOldBoss;
	}

	public static String getOldName() {
		return oldName;
	}

	public static void setOldName(String oldName) {
		QuanLyPhongBan.oldName = oldName;
	}

}
