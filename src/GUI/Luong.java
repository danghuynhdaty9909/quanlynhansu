package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class Luong extends JFrame {

	private JPanel contentPane;
	private JTextField tf;
	private JTable table_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Luong frame = new Luong();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Luong() {
		setTitle("Quản Lý Lương Nhân Sự");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBackground(Color.LIGHT_GRAY);
		tabbedPane.setBounds(0, 0, 1350, 729);
		contentPane.add(tabbedPane);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		tabbedPane.addTab("Thực lãnh", null, panel_2, null);
		panel_2.setLayout(null);
		
		JLabel lb_1 = new JLabel("Mã Nhân Viên:");
		lb_1.setBackground(new Color(0, 0, 0));
		lb_1.setForeground(Color.BLACK);
		lb_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lb_1.setBounds(429, 164, 115, 20);
		panel_2.add(lb_1);
		
		tf = new JTextField();
		tf.setBounds(554, 166, 155, 20);
		panel_2.add(tf);
		tf.setColumns(10);
		
		JLabel lb = new JLabel("");
		lb.setIcon(new ImageIcon("img\\search.png"));
		lb.setBounds(719, 152, 48, 48);
		panel_2.add(lb);
		
		JPanel pn= new JPanel();
		pn.setBounds(0, 313, 1345, 348);
		panel_2.add(pn);
		pn.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		pn.add(scrollPane_1, BorderLayout.NORTH);
		
		table_2 = new JTable();
		table_2.setBackground(Color.WHITE);
		table_2.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, "", null, ""},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
			},
			new String[] {
				"M\u00E3 Nh\u00E2n Vi\u00EAn", "L\u01B0\u01A1ng C\u0103n B\u1EA3n", "L\u01B0\u01A1ng Ph\u1EE5 C\u1EA5p", "L\u01B0\u01A1ng Th\u01B0\u1EDFng", "S\u1ED1 Ng\u00E0y L\u00E0m", "L\u01B0\u01A1ng Th\u1EF1c L\u00E3nh"
			}
		));
		scrollPane_1.setViewportView(table_2);
		
		JLabel lb_2= new JLabel("Tính Lương Thực Lãnh");
		lb_2.setFont(new Font("SansSerif", Font.BOLD, 25));
		lb_2.setHorizontalAlignment(SwingConstants.CENTER);
		lb_2.setBounds(110, 39, 1126, 59);
		panel_2.add(lb_2);
		
		JLabel lblThng = new JLabel("Tháng:");
		lblThng.setFont(new Font("Garamond", Font.BOLD, 15));
		lblThng.setBounds(829, 165, 48, 20);
		panel_2.add(lblThng);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Tahoma", Font.BOLD, 11));
		comboBox.setForeground(Color.BLACK);
		comboBox.setBackground(Color.WHITE);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}));
		comboBox.setBounds(887, 166, 78, 20);
		panel_2.add(comboBox);
		
		JLabel lblBngLng = new JLabel("Bảng Lương");
		lblBngLng.setFont(new Font("Times New Roman", Font.BOLD, 23));
		lblBngLng.setHorizontalAlignment(SwingConstants.CENTER);
		lblBngLng.setBounds(594, 260, 158, 48);
		panel_2.add(lblBngLng);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("img\\return.png"));
		lblNewLabel.setBounds(1250, 11, 85, 59);
		lblNewLabel.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new MainFrame().setVisible(true);
				
			}
		});
		panel_2.add(lblNewLabel);
		
		
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}

