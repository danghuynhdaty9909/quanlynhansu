package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import DTO.NhanVienDTO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class DSNhanVien extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTable table;
	private DefaultTableModel model;
	private JButton btOk;
	private JButton btClose;
	private static String newIdBoss;
	private static String nameBoss = "";
	PhongBanBUS bus = new PhongBanBUS();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DSNhanVien frame = new DSNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DSNhanVien() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 93, 1330, 387);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setFont(new Font("SansSerif", Font.BOLD, 12));
		table.setModel(model=new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"M\u00E3 Nh\u00E2n Vi\u00EAn", "H\u1ECD", "T\u00EAn", "Gi\u1EDBi T\u00EDnh", "Email", "S\u1ED1 \u0110i\u1EC7n Tho\u1EA1i", "S\u1ED1 CMND", "Qu\u00EA Qu\u00E1n", "M\u00E3 ph\u00F2ng ban", "M\u00E3 ch\u1EE9c v\u1EE5", "M\u00E3 h\u1EE3p \u0111\u1ED3ng", "M\u00E3 TDHV"
			}
		));
		scrollPane.setViewportView(table);

		JLabel lblNewLabel = new JLabel("Danh Sách Nhân Viên");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 30));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(89, 11, 1163, 71);
		contentPane.add(lblNewLabel);

		btOk = new JButton("Đồng Ý");
		btOk.setBounds(436, 529, 89, 23);
		contentPane.add(btOk);

		btClose = new JButton("Đóng");
		btClose.setBounds(835, 529, 89, 23);
		contentPane.add(btClose);
		btOk.addActionListener(this);
		btClose.addActionListener(this);
		loadData();
	}

	public void loadData() {
		NhanVienBUS staffBus=new NhanVienBUS();
		ArrayList<NhanVienDTO> list = staffBus.loadDataByCondition("", "");
		Vector<String> row;
		
		for (NhanVienDTO staff : list) {
			row = new Vector<>();
			row.add(staff.getId());
			row.add(staff.getFirstName());
			row.add(staff.getLastName());
			row.add(staff.getSex());
			row.add(staff.getMail());
			row.add(staff.getPhoneNumber());
			row.add(staff.getIdCard());
			row.add(staff.getAddress());
			row.add(staff.getIdDepartment());
			row.add(staff.getIdPosition());
			row.add(staff.getIdContract());
			row.add(staff.getIdLiteracy());
			model.addRow(row);
		}
		table.setModel(model);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btClose){
			new ThemPhongBan().setVisible(true);
			setVisible(false);
		}
		String oldName = ThemPhongBan.getDto().getName();// lấy lại dữ liệu
															// trước khi nhân
															// chọn trưởng phòng
		String oldAddress = ThemPhongBan.getDto().getAddress();
		String newid=ThemPhongBan.getDto().getId();
		if (e.getSource() == btOk) {
			boolean flag = false;
			do {
				if (table.getSelectedRow() < 0) {
					JOptionPane.showMessageDialog(null, "Nhấn chọn dòng chứa thông tin nhân viên làm trưởng phòng!!!");
				} else {
					int k = table.getSelectedRow();
					String idBoss = (String) getModel().getValueAt(k, 0);// lấy
																			// mã
																			// nhân
																			// viên
																			// là
																			// trưởng
																			// phòng
																			// mới
					//PhongBanBUS bus = new PhongBanBUS();
					ArrayList<String> list = bus.getListOneCol("maphongban", "", "");
					int i=0;
					for (String id : list) {
						if (id.equals(idBoss)) {
							JOptionPane.showMessageDialog(null,
									"Nhân viên này đã là trưởng phòng của phòng khác! Vui lòng chọn lại!");
									i=1;
									break;
						} 
					}
					if(i==0){
							flag = true;
							nameBoss = (String) getModel().getValueAt(k, 1) + " " + (String) getModel().getValueAt(k, 2);
							ThemPhongBan.getDto().setIdBoss(idBoss);
							ThemPhongBan add = new ThemPhongBan();
							ThemPhongBan.getDto().setIdBoss(idBoss);
							ThemPhongBan.getDto().setId(newid);
							ThemPhongBan.getDto().setName(oldName);
							ThemPhongBan.getDto().setAddress(oldAddress);
							add.getTxtBoss().setText(nameBoss);
							add.getTxtName().setText(oldName);
							add.getTxtAddress().setText(oldAddress);
							newIdBoss=idBoss;
							add.setVisible(true);
							setVisible(false);
					}
					
				}
			} while (!flag);
		}
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public DefaultTableModel getModel() {
		return model;
	}

	public void setModel(DefaultTableModel model) {
		this.model = model;
	}

	public static String getNameBoss() {
		return nameBoss;
	}

	public static void setNameBoss(String nameBoss) {
		DSNhanVien.nameBoss = nameBoss;
	}

	public static String getNewIdBoss() {
		return newIdBoss;
	}

	public static void setNewIdBoss(String newIdBoss) {
		DSNhanVien.newIdBoss = newIdBoss;
	}

}
