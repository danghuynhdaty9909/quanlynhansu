package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.ChucVuBUS;
import BUS.HopDongLaoDongBUS;
import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import BUS.TrinhDoHocVanBUS;
import DTO.NhanVienDTO;

import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Toolkit;
import javax.swing.DefaultComboBoxModel;

@SuppressWarnings("serial")
public class XoaNhanVien extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField txtSearch;
	private JButton btnDelete;
	private JLabel lbReturn;
	@SuppressWarnings("rawtypes")
	private JComboBox cbSearchInput;
	private JLabel lbSearchInput;
	private JLabel lbSearch;
	private DefaultTableModel model;
	private static String idContract;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					XoaNhanVien frame = new XoaNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public XoaNhanVien() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\cancel2.png"));
		setTitle("Xóa Nhân Viên");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 366, 1350, 339);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setModel(model = new DefaultTableModel(new Object[][] {},
				new String[] { "M\u00E3 Nh\u00E2n Vi\u00EAn", "H\u1ECD", "T\u00EAn", "Gi\u1EDBi T\u00EDnh", "Email",
						"S\u1ED1 \u0110i\u1EC7n Tho\u1EA1i", "S\u1ED1 CMND", "\u0110\u1ECBa Ch\u1EC9",
						"D\u00E2n t\u1ED9c", "M\u00E3 ph\u00F2ng ban", "M\u00E3 Ch\u1EE9c V\u1EE5", "M\u00E3 HDLD",
						"M\u00E3TDHV" }));
		table.setRowHeight(25);
		scrollPane.setViewportView(table);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(179, 0, 1150, 363);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		lbSearchInput = new JLabel("TÌM KIẾM NHÂN SỰ:");
		lbSearchInput.setFont(new Font("Tahoma", Font.BOLD, 12));
		lbSearchInput.setBounds(190, 160, 132, 25);
		panel_1.add(lbSearchInput);

		txtSearch = new JTextField();
		txtSearch.setBounds(332, 160, 242, 25);
		panel_1.add(txtSearch);
		txtSearch.setColumns(10);

		String[] listSearch = { "Tìm kiếm theo Email", "Tìm kiếm theo Mã Chức Vụ", "Tìm kiếm theo Mã Phòng Ban",
				"Tìm kiếm theo Tên Nhân Viên", "Tìm kiếm theo Mã Nhân Viên" };
		cbSearchInput = new JComboBox(listSearch);
		cbSearchInput.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cbSearchInput.setSelectedIndex(3);
		cbSearchInput.setBounds(599, 160, 232, 21);
		panel_1.add(cbSearchInput);

		lbSearch = new JLabel("");
		lbSearch.setIcon(new ImageIcon("img\\search.png"));
		lbSearch.setBounds(839, 143, 48, 42);
		lbSearch.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {
				String[] input = new String[2];
				input[1] = txtSearch.getText();
				String cond = (String) cbSearchInput.getSelectedItem();
				switch (cond) {
				case "Tìm kiếm theo Mã Nhân Viên":
					input[0] = "1";
					break;
				case "Tìm kiếm theo Tên Nhân Viên":
					input[0] = "2";
					break;
				case "Tìm kiếm theo Mã Phòng Ban":
					input[0] = "3";
					break;
				case "Tìm kiếm theo Mã Chức Vụ":
					input[0] = "4";
					break;
				case "Tìm kiếm theo Email":
					input[0] = "5";
					break;
				}
				NhanVienBUS staffBus = new NhanVienBUS();
				ArrayList<NhanVienDTO> list = staffBus.searchStaff(input);
				Vector<String> row;
				if (list.isEmpty()) {
					JOptionPane.showMessageDialog(null, "Không tìm thấy nhân viên!!");
				} else {
					for (NhanVienDTO staff : list) {
						row = new Vector<>();
						row.add(staff.getId());
						row.add(staff.getFirstName());
						row.add(staff.getLastName());
						row.add(staff.getSex());
						row.add(staff.getMail());
						row.add(staff.getPhoneNumber());
						row.add(staff.getIdCard());
						row.add(staff.getAddress());
						row.add(staff.getNation());
						row.add(staff.getIdDepartment());
						row.add(staff.getIdPosition());
						row.add(staff.getIdContract());
						row.add(staff.getIdLiteracy());
						model.addRow(row);
					}
					table.setModel(model);
				}

			}

			@Override
			public void mouseExited(MouseEvent e) {
				lbSearch.setBorder(null);

			}

			@Override
			public void mouseEntered(MouseEvent e) {

				lbSearch.setBorder(new LineBorder(Color.gray));

			}

			@Override
			public void mouseClicked(MouseEvent e) {

			}
		});
		panel_1.add(lbSearch);

		btnDelete = new JButton("XÓA");
		btnDelete.setIcon(new ImageIcon("img\\cancel2.png"));
		btnDelete.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnDelete.setBounds(458, 253, 151, 60);
		panel_1.add(btnDelete);

		JLabel lbDeleteStaff = new JLabel("XÓA NHÂN VIÊN");
		lbDeleteStaff.setForeground(Color.RED);
		lbDeleteStaff.setFont(new Font("Times New Roman", Font.BOLD, 30));
		lbDeleteStaff.setHorizontalAlignment(SwingConstants.CENTER);
		lbDeleteStaff.setBounds(332, 56, 411, 60);
		panel_1.add(lbDeleteStaff);

		lbReturn = new JLabel("");
		lbReturn.setIcon(new ImageIcon("img\\return.png"));
		lbReturn.setBounds(1051, 11, 89, 60);
		panel_1.add(lbReturn);

		clickXoa();

		clickReturn();

	}

	public void clickXoa() {
		btnDelete.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int k = table.getSelectedRow();
				XuatXoaNV delete = new XuatXoaNV();
				if (k >= 0) {
					NhanVienBUS staffBus = new NhanVienBUS();
					NhanVienDTO staff = staffBus.getStaff((String) model.getValueAt(k, 0));
					PhongBanBUS departmentBus = new PhongBanBUS();
					ChucVuBUS positionBus = new ChucVuBUS();
					HopDongLaoDongBUS contractBus = new HopDongLaoDongBUS();
					TrinhDoHocVanBUS literacyBus = new TrinhDoHocVanBUS();
					if (staff != null) {
						delete.getTxtFirstName().setText(staff.getFirstName());
						delete.getTxtLastName().setText(staff.getLastName());
						delete.getTxtSex().setText(staff.getSex());
						delete.getTxtEmail().setText(staff.getMail());
						delete.getTextNation().setText(staff.getNation());
						delete.getTxtAddress().setText(staff.getAddress());
						delete.getTxtId().setText(staff.getId());
						delete.getTxtPhoneNumber().setText(staff.getPhoneNumber());
						delete.getTxtAccount().setText(staff.getAccount());
						delete.getTxtIdcard().setText(staff.getIdCard());
						delete.getTxtBirthday().setText(staff.getBirthday());
						delete.getTxtDepartment()
								.setText(departmentBus.getNameDepartment((String) model.getValueAt(k, 9)));
						delete.getTxtPosition().setText(positionBus.getNameById((String) model.getValueAt(k, 10)));
						delete.getTxtContract().setText(contractBus.getName((String) model.getValueAt(k, 11)));
						idContract = (String) model.getValueAt(k, 11);
						delete.getTxtLiteracy()
								.setText(literacyBus.getNameById("tentdhv", (String) model.getValueAt(k, 12)));
						setVisible(false);
					}
					delete.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Vui lòng chọn vào dòng chứa nhân viên cần xóa rồi bấm xóa!!!");
				}

			}
		});
	}

	public void clickReturn() {
		lbReturn.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new QuanLyNhanSu().setVisible(true);

			}
		});
	}

	public static String getIdContract() {
		return idContract;
	}

	public static void setIdContract(String idContract) {
		XoaNhanVien.idContract = idContract;
	}

}
