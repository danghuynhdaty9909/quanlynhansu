package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import DTO.DepartmentDTO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.Color;
import javax.swing.JButton;

public class ThemPhongBan extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtBoss;
	private JTextField txtAddress;
	private JButton btAdd;
	private JButton btCancel;
	private JButton btGetBoss;
	private static DepartmentDTO dto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ThemPhongBan frame = new ThemPhongBan();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ThemPhongBan() {
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\trung\\workspace\\GUI_Giaodien\\GUI_Giaodien\\img\\add1.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 659, 415);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);

		JLabel lbName = new JLabel("Tên Phòng Ban:");
		lbName.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbName.setBounds(117, 68, 125, 33);
		contentPane.add(lbName);

		JLabel lbBoss = new JLabel("Trưởng Phòng:");
		lbBoss.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbBoss.setBounds(117, 103, 125, 33);
		contentPane.add(lbBoss);

		JLabel lbAddress = new JLabel("Địa Chỉ Phòng:");
		lbAddress.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbAddress.setBounds(117, 145, 125, 33);
		contentPane.add(lbAddress);

		txtName = new JTextField();
		txtName.setBounds(280, 76, 265, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);

		txtBoss = new JTextField();
		txtBoss.setBounds(280, 111, 220, 20);
		contentPane.add(txtBoss);
		txtBoss.setColumns(10);

		txtAddress = new JTextField();
		txtAddress.setBounds(280, 153, 265, 20);
		contentPane.add(txtAddress);
		txtAddress.setColumns(10);

		JLabel lbTitle = new JLabel("Thêm Phòng Ban");
		lbTitle.setForeground(Color.GREEN);
		lbTitle.setBackground(new Color(0, 100, 0));
		lbTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbTitle.setBounds(20, 11, 623, 33);
		contentPane.add(lbTitle);

		btAdd = new JButton("Thêm");
		btAdd.setBackground(Color.WHITE);
		btAdd.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btAdd.setBounds(205, 228, 86, 26);
		contentPane.add(btAdd);

		btCancel = new JButton("Hủy");
		btCancel.setBackground(Color.WHITE);
		btCancel.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btCancel.setBounds(423, 228, 77, 26);
		btCancel.addActionListener(this);
		contentPane.add(btCancel);

		btGetBoss = new JButton("New button");
		btGetBoss.setBackground(Color.WHITE);
		btGetBoss.setFont(new Font("Serif", Font.BOLD, 15));
		btGetBoss.setBounds(510, 110, 35, 20);
		contentPane.add(btGetBoss);
		btAdd.addActionListener(this);
		btGetBoss.addActionListener(this);
		dto = new DepartmentDTO();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btGetBoss) {
			if (txtName.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Tên phòng không thể rỗng!!!");
			} else {
				if (txtAddress.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Địa chỉ phòng không thể rỗng!!!");
				} else {
					dto.setAddress(txtAddress.getText());
					dto.setName(txtName.getText());
					PhongBanBUS bus = new PhongBanBUS();
					ArrayList<String> list = bus.getListOneCol("maphongban", "", "");
					int max = 0, temp = 0;// lấy mã mới
					if (list != null) {
						for (String st : list) {
							System.out.println(st);
							temp = Integer.parseInt(st.substring(1));
							if (temp > max) {
								max = temp;
							}
						}
					}
					dto.setId(String.valueOf("P" + (max + 1)));
					dto.setIdBoss("");
					setVisible(false);
					DSNhanVien ds = new DSNhanVien();
					dto.setIdBoss(DSNhanVien.getNewIdBoss());
					ds.setVisible(true);
				}
			}
		}
		if (e.getSource() == btCancel) {
			setVisible(false);
			new QuanLyPhongBan().setVisible(true);
		}
		if (e.getSource() == btAdd) {
			if (txtBoss.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Nhấn nút bên phải khung Trưởng phòng để chon trưởng phòng!!!");
			} else {
				PhongBanBUS bus = new PhongBanBUS();
				NhanVienBUS busStaff = new NhanVienBUS();
				String idPosition = busStaff.getId("macv", "manv", getDto().getIdBoss());
				if (idPosition.equals("CEO") || idPosition.equals( "TP")) {
					JOptionPane.showMessageDialog(null, "Nhân viên này đã là trưởng phòng của phòng khác!!!");
				} else {
					boolean flag_1 = bus.insert(getDto());
					
					if (flag_1) {
						boolean flag_2 = busStaff.updateOneCol("macv", "TP", "manv", getDto().getIdBoss());
						boolean flag_3 = busStaff.updateOneCol("maphongban", getDto().getId(), "manv", getDto().getIdBoss());
						if(flag_2 && flag_3){
							
							JOptionPane.showMessageDialog(null, "Thêm thành công!!!");
						}
					} else {
						JOptionPane.showMessageDialog(null, "Thêm thất bại!!!");
					}
				}

			}
		}
	}

	public static DepartmentDTO getDto() {
		return dto;
	}

	public static void setDto(DepartmentDTO dto) {
		ThemPhongBan.dto = dto;
	}

	public JTextField getTxtName() {
		return txtName;
	}

	public void setTxtName(JTextField txtName) {
		this.txtName = txtName;
	}

	public JTextField getTxtBoss() {
		return txtBoss;
	}

	public void setTxtBoss(JTextField txtBoss) {
		this.txtBoss = txtBoss;
	}

	public JTextField getTxtAddress() {
		return txtAddress;
	}

	public void setTxtAddress(JTextField txtAddress) {
		this.txtAddress = txtAddress;
	}

	public JButton getBtAdd() {
		return btAdd;
	}

	public void setBtAdd(JButton btAdd) {
		this.btAdd = btAdd;
	}

	public JButton getBtCancel() {
		return btCancel;
	}

	public void setBtCancel(JButton btCancel) {
		this.btCancel = btCancel;
	}

	public JButton getBtGetBoss() {
		return btGetBoss;
	}

	public void setBtGetBoss(JButton btGetBoss) {
		this.btGetBoss = btGetBoss;
	}

}
