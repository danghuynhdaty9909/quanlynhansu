package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import BUS.HopDongLaoDongBUS;
import BUS.NhanVienBUS;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.border.BevelBorder;
import javax.swing.border.MatteBorder;

public class XuatXoaNV extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtEmail;
	private JTextField txtId;
	private JTextField txtPhoneNumber;
	private JTextField txtIdcard;
	private JTable table;
	private JTable table_1;
	private JTable table_2;
	private JLabel lbCancel;
	private JLabel lbOk;
	private JTextField textNation;
	private JTextField txtAddress;
	private JTextField txtAccount;
	private JLabel lbPhoneNumber;
	private JTextField txtSex;
	private JTextField txtContract;
	private JTextField txtLiteracy;
	private JTextField txtDepartment;
	private JTextField txtPosition;
	private JTextField txtBirthday;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					XuatXoaNV frame = new XuatXoaNV();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public XuatXoaNV() {
		setTitle("Xóa Nhân Viên");
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\officer.png"));
		setBounds(100, 100, 1366, 768);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setForeground(Color.BLACK);
		panel.setBounds(149, 0, 1055, 729);
		panel.setLayout(null);

		JLabel lbFirstName = new JLabel("Họ:");
		lbFirstName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbFirstName.setBounds(170, 207, 30, 20);
		panel.add(lbFirstName);

		txtFirstName = new JTextField();
		txtFirstName.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		txtFirstName.setBounds(221, 207, 97, 23);
		txtFirstName.setBorder(new BevelBorder(BevelBorder.RAISED, new Color(192, 192, 192), null, null, null));
		panel.add(txtFirstName);
		txtFirstName.setColumns(10);

		JLabel lbLastName = new JLabel("Tên:");
		lbLastName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbLastName.setBounds(340, 207, 46, 20);
		panel.add(lbLastName);

		txtLastName = new JTextField();
		txtLastName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtLastName.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		txtLastName.setBounds(413, 207, 121, 23);
		panel.add(txtLastName);
		txtLastName.setColumns(10);

		JLabel lbId = new JLabel("Mã Nhân Viên:");
		lbId.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbId.setBounds(170, 285, 112, 20);
		panel.add(lbId);

		txtEmail = new JTextField();
		txtEmail.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtEmail.setFont(new Font("SansSerif", Font.PLAIN, 12));
		txtEmail.setBounds(319, 246, 215, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);

		JLabel lbNation = new JLabel("Dân tộc:");
		lbNation.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbNation.setBounds(605, 245, 61, 20);
		panel.add(lbNation);

		JLabel lbEmail = new JLabel("Email:");
		lbEmail.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbEmail.setBounds(170, 246, 46, 20);
		panel.add(lbEmail);

		lbPhoneNumber = new JLabel("Số Điện Thoại:");
		lbPhoneNumber.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbPhoneNumber.setBounds(170, 324, 105, 20);
		panel.add(lbPhoneNumber);

		JLabel lbIdcard = new JLabel("Số CMND:");
		lbIdcard.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbIdcard.setBounds(170, 363, 78, 20);
		panel.add(lbIdcard);

		JLabel lbDepartmentName = new JLabel("Tên Phòng Ban:");
		lbDepartmentName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbDepartmentName.setBounds(170, 442, 121, 20);
		panel.add(lbDepartmentName);

		JLabel lbPositionName = new JLabel("Tên Chức Vụ: ");
		lbPositionName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbPositionName.setBounds(605, 442, 105, 20);
		panel.add(lbPositionName);

		JLabel lbContractName = new JLabel("Tên HDLD:");
		lbContractName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbContractName.setBounds(605, 363, 78, 20);
		panel.add(lbContractName);

		JLabel lbLiteracyName = new JLabel("Tên TDHV:");
		lbLiteracyName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbLiteracyName.setBounds(605, 403, 105, 20);
		panel.add(lbLiteracyName);

		txtId = new JTextField();
		txtId.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtId.setBounds(319, 285, 215, 20);
		panel.add(txtId);
		txtId.setColumns(10);

		txtPhoneNumber = new JTextField();
		txtPhoneNumber.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtPhoneNumber.setBounds(319, 324, 215, 20);
		panel.add(txtPhoneNumber);
		txtPhoneNumber.setColumns(10);

		txtIdcard = new JTextField();
		txtIdcard.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtIdcard.setBounds(319, 363, 215, 20);
		panel.add(txtIdcard);
		txtIdcard.setColumns(10);

		JLabel lbAddress = new JLabel("Địa Chỉ:");
		lbAddress.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbAddress.setBounds(605, 285, 78, 20);
		panel.add(lbAddress);

		JLabel lbSex = new JLabel("Giới Tính:");
		lbSex.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbSex.setBounds(605, 207, 82, 20);
		panel.add(lbSex);

		table = new JTable();
		table.setBounds(1340, 699, -1043, -138);
		panel.add(table);

		getContentPane().add(panel);

		lbCancel = new JLabel("Hủy");
		lbCancel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbCancel.setIcon(new ImageIcon("img\\cancel.png"));
		lbCancel.setBounds(659, 608, 117, 72);
		panel.add(lbCancel);

		lbOk = new JLabel("Đồng Ý");
		lbOk.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbOk.setIcon(new ImageIcon("img\\accept.png"));
		lbOk.setBounds(281, 608, 121, 72);
		panel.add(lbOk);

		textNation = new JTextField();
		textNation.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		textNation.setBounds(703, 247, 200, 20);
		panel.add(textNation);
		textNation.setColumns(10);

		txtAddress = new JTextField();
		txtAddress.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtAddress.setBounds(703, 285, 200, 20);
		panel.add(txtAddress);
		txtAddress.setColumns(10);

		JLabel lbBirthday = new JLabel("Ngày Sinh: ");
		lbBirthday.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbBirthday.setBounds(170, 403, 136, 23);
		panel.add(lbBirthday);

		JLabel lbAccount = new JLabel("Số Tài Khoản:");
		lbAccount.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbAccount.setBounds(605, 324, 105, 14);
		panel.add(lbAccount);

		txtAccount = new JTextField();
		txtAccount.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtAccount.setBounds(703, 324, 200, 20);
		panel.add(txtAccount);
		txtAccount.setColumns(10);

		JLabel lbImg = new JLabel("New label");
		lbImg.setBorder(new MatteBorder(0, 0, 0, 0, (Color) Color.LIGHT_GRAY));
		lbImg.setIcon(new ImageIcon("img\\BillGates.jpeg"));
		lbImg.setBounds(200, 67, 112, 112);
		panel.add(lbImg);

		JLabel lbStaffInfo = new JLabel("Thông Tin Nhân Viên");
		lbStaffInfo.setForeground(new Color(139, 0, 0));
		lbStaffInfo.setHorizontalAlignment(SwingConstants.CENTER);
		lbStaffInfo.setFont(new Font("Times New Roman", Font.BOLD, 35));
		lbStaffInfo.setBounds(400, 98, 376, 55);
		panel.add(lbStaffInfo);

		txtSex = new JTextField();
		txtSex.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtSex.setBounds(703, 209, 200, 20);
		panel.add(txtSex);
		txtSex.setColumns(10);

		txtContract = new JTextField();
		txtContract.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtContract.setBounds(703, 363, 200, 20);
		panel.add(txtContract);
		txtContract.setColumns(10);

		txtLiteracy = new JTextField();
		txtLiteracy.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtLiteracy.setBounds(703, 403, 200, 20);
		panel.add(txtLiteracy);
		txtLiteracy.setColumns(10);

		txtDepartment = new JTextField();
		txtDepartment.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtDepartment.setBounds(319, 444, 215, 20);
		panel.add(txtDepartment);
		txtDepartment.setColumns(10);

		txtPosition = new JTextField();
		txtPosition.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtPosition.setBounds(703, 444, 200, 20);
		panel.add(txtPosition);
		txtPosition.setColumns(10);

		txtBirthday = new JTextField();
		txtBirthday.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtBirthday.setBounds(319, 403, 215, 20);
		panel.add(txtBirthday);
		txtBirthday.setColumns(10);

		clickTuChoi();
		clickDongY();

	}

	public void clickTuChoi() {
		lbCancel.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {
				lbCancel.setForeground(null);

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lbCancel.setForeground(Color.red);

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new XoaNhanVien().setVisible(true);

			}
		});

	}

	public void clickDongY() {
		lbOk.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {
				lbOk.setForeground(null);

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lbOk.setForeground(Color.green);

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int respone = JOptionPane.showConfirmDialog(null, "Bạn có chắc chắn muốn xóa nhân viên này ? ",
						"Confirm", JOptionPane.YES_NO_OPTION);
				if (respone == JOptionPane.YES_OPTION) {
					if (new NhanVienBUS().deleteStaff(txtId.getText())
							&& new HopDongLaoDongBUS().delete(XoaNhanVien.getIdContract())) {
						JOptionPane.showMessageDialog(null, "Xóa thành công!");
					} else {
						JOptionPane.showMessageDialog(null, "Xóa thất bại!!!");
					}

				}
			}
		});
	}

	public JTextField getTxtSex() {
		return txtSex;
	}

	public void setTxtSex(JTextField txtSex) {
		this.txtSex = txtSex;
	}

	public JTextField getTxtContract() {
		return txtContract;
	}

	public void setTxtContract(JTextField txtContract) {
		this.txtContract = txtContract;
	}

	public JTextField getTxtLiteracy() {
		return txtLiteracy;
	}

	public void setTxtLiteracy(JTextField txtLiteracy) {
		this.txtLiteracy = txtLiteracy;
	}

	public JTextField getTxtDepartment() {
		return txtDepartment;
	}

	public void setTxtDepartment(JTextField txtDepartment) {
		this.txtDepartment = txtDepartment;
	}

	public JTextField getTxtPosition() {
		return txtPosition;
	}

	public void setTxtPosition(JTextField txtPosition) {
		this.txtPosition = txtPosition;
	}

	public JTextField getTxtBirthday() {
		return txtBirthday;
	}

	public void setTxtBirthday(JTextField txtBirthday) {
		this.txtBirthday = txtBirthday;
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JTextField getTxtFirstName() {
		return txtFirstName;
	}

	public void setTxtFirstName(JTextField txtFirstName) {
		this.txtFirstName = txtFirstName;
	}

	public JTextField getTxtLastName() {
		return txtLastName;
	}

	public void setTxtLastName(JTextField txtLastName) {
		this.txtLastName = txtLastName;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}

	public JTextField getTxtId() {
		return txtId;
	}

	public void setTxtId(JTextField txtId) {
		this.txtId = txtId;
	}

	public JTextField getTxtPhoneNumber() {
		return txtPhoneNumber;
	}

	public void setTxtPhoneNumber(JTextField txtPhoneNumber) {
		this.txtPhoneNumber = txtPhoneNumber;
	}

	public JTextField getTxtIdcard() {
		return txtIdcard;
	}

	public void setTxtIdcard(JTextField txtIdcard) {
		this.txtIdcard = txtIdcard;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JTable getTable_1() {
		return table_1;
	}

	public void setTable_1(JTable table_1) {
		this.table_1 = table_1;
	}

	public JTable getTable_2() {
		return table_2;
	}

	public void setTable_2(JTable table_2) {
		this.table_2 = table_2;
	}

	public JLabel getLbTuChoi() {
		return lbCancel;
	}

	public void setLbTuChoi(JLabel lbTuChoi) {
		this.lbCancel = lbTuChoi;
	}

	public JLabel getLbDongY() {
		return lbOk;
	}

	public void setLbDongY(JLabel lbDongY) {
		this.lbOk = lbDongY;
	}

	public JTextField getTextNation() {
		return textNation;
	}

	public void setTextNation(JTextField textNation) {
		this.textNation = textNation;
	}

	public JTextField getTxtAddress() {
		return txtAddress;
	}

	public void setTxtAddress(JTextField txtAddress) {
		this.txtAddress = txtAddress;
	}

	public JTextField getTxtAccount() {
		return txtAccount;
	}

	public void setTxtAccount(JTextField txtAccount) {
		this.txtAccount = txtAccount;
	}

	public JLabel getLbPhoneNumber() {
		return lbPhoneNumber;
	}

	public void setLbPhoneNumber(JLabel lbPhoneNumber) {
		this.lbPhoneNumber = lbPhoneNumber;
	}
}
