package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

public class TKMucLuong extends JFrame {

	private JPanel contentPane;
	private JProgressBar jb1;
	private JProgressBar jb2;
	private JProgressBar jb3;
	private JProgressBar jb4;
	private JProgressBar jb5;
	private int ml1=5;
	private int ml2=10;
	private int ml3=15;
	private int ml4=20;
	private int ml5=30;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TKMucLuong frame = new TKMucLuong();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TKMucLuong() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100,1366, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(220, 0, 915, 729);
		contentPane.add(panel);
		panel.setLayout(null);
		
		int tong=ml1+ml2+ml3+ml4+ml5;
		int pcMl1=ml1*100/tong;
		int pcMl2=ml2*100/tong;
		int pcMl3=ml3*100/tong;
		int pcMl4=ml4*100/tong;
		int pcMl5=ml5*100/tong;
		
		JLabel lblNewLabel = new JLabel("Thống Kê Lương");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 25));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(0, 39, 915, 80);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("5 -> 10 triệu vnd:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(175, 226, 132, 26);
		panel.add(lblNewLabel_1);
		
		JLabel label = new JLabel("10 -> 15 triệu vnd: ");
		label.setFont(new Font("Tahoma", Font.BOLD, 15));
		label.setBounds(175, 315, 156, 26);
		panel.add(label);
		
		JLabel lblNewLabel_2 = new JLabel("15 -> 20 triệu vnd");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2.setBounds(175, 399, 142, 26);
		panel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("20 -> 25 triệu vnd");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_3.setBounds(175, 485, 142, 26);
		panel.add(lblNewLabel_3);
		
		JLabel label_1 = new JLabel("25 -> 30 triệu vnd");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_1.setBounds(175, 575, 142, 26);
		panel.add(label_1);
		
		jb1 = new JProgressBar(0, 100);
		jb1.setForeground(Color.BLACK);
		jb1.setValue(pcMl1);
		jb1.setStringPainted(true);
		jb1.setBounds(356, 224, 348, 38);
		panel.add(jb1);
		
		jb2 = new JProgressBar(0, 100);
		jb2.setForeground(Color.BLACK);
		jb2.setValue(pcMl2);
		jb2.setStringPainted(true);
		jb2.setBounds(356, 303, 348, 38);
		panel.add(jb2);
		
		jb3 = new JProgressBar(0, 100);
		jb3.setForeground(Color.BLACK);
		jb3.setValue(pcMl3);
		jb3.setStringPainted(true);
		jb3.setBounds(356, 387, 348, 38);
		panel.add(jb3);
		
		jb4 = new JProgressBar(0, 100);
		jb4.setForeground(Color.BLACK);
		jb4.setValue(pcMl4);
		jb4.setStringPainted(true);
		jb4.setBounds(356, 473, 348, 38);
		panel.add(jb4);
		
		jb5 = new JProgressBar(0, 100);
		jb5.setForeground(Color.BLACK);
		jb5.setValue(pcMl5);
		jb5.setStringPainted(true);
		jb5.setBounds(356, 563, 348, 38);
		panel.add(jb5);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4"}));
		comboBox.setBounds(632, 152, 72, 20);
		panel.add(comboBox);
		
		JLabel lblNewLabel_4 = new JLabel("Lương:");
		lblNewLabel_4.setForeground(Color.BLUE);
		lblNewLabel_4.setFont(new Font("SansSerif", Font.BOLD, 15));
		lblNewLabel_4.setBounds(567, 152, 55, 17);
		panel.add(lblNewLabel_4);
		
		JButton btHuy = new JButton("Đóng");
		btHuy.setForeground(Color.RED);
		btHuy.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btHuy.setBounds(404, 659, 110, 32);
		btHuy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new ThongKeNhanVien().setVisible(true);
				
			}
		});
		panel.add(btHuy);
	}
}
