package GUI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import BUS.ChucVuBUS;
import BUS.HopDongLaoDongBUS;
import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import BUS.TrinhDoHocVanBUS;
import DAO.TrinhDoHocVanDAO;
import DTO.ContractDTO;
import DTO.LiteracyDTO;
import DTO.NhanVienDTO;

import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import javax.swing.SwingConstants;
import javax.swing.DefaultComboBoxModel;

@SuppressWarnings("serial")
public class ThemNhanVien extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtEmail;
	private JTextField txtId;
	private JTextField txtPhoneNumber;
	private JTextField txtIdCard;
	private JTable table;
	private JTable table_1;
	private JTable table_2;
	private JLabel lbAdd;
	private JLabel lbCancel;
	private JLabel lbReturn;
	private JTextField txtNation;
	private JTextField txtAddress;
	private JButton btAddImage;
	private String linkFile;
	private JLabel lbImg;
	private JRadioButton rbtFemale;
	private JRadioButton rbtMale;
	private JTextField txtAccount;
	private JComboBox<String> cbDepartmentName;
	private JComboBox<String> cbMonth;
	private JComboBox<String> cbDay;
	private JComboBox<String> cbYear;
	private JComboBox<String> cbPositionName;
	@SuppressWarnings("rawtypes")
	private JComboBox cbLiteracyName;
	@SuppressWarnings("rawtypes")
	private JComboBox cbContract;
	private JTextField txtSpecialized;
	private TrinhDoHocVanBUS literacyBus = new TrinhDoHocVanBUS();
	private ChucVuBUS positionBus = new ChucVuBUS();
	private PhongBanBUS departmentBus = new PhongBanBUS();
	private HopDongLaoDongBUS contractBus = new HopDongLaoDongBUS();
	private NhanVienBUS staffBus = new NhanVienBUS();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ThemNhanVien frame = new ThemNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ThemNhanVien() {
		setTitle("THÊM NHÂN SỰ");
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\add1.png"));
		setBounds(100, 100, 1366, 768);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setForeground(Color.BLACK);
		panel.setBounds(110, -14, 1091, 718);
		panel.setLayout(null);

		JLabel lbFirstName = new JLabel("Họ:");
		lbFirstName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbFirstName.setBounds(310, 160, 30, 20);
		panel.add(lbFirstName);

		txtFirstName = new JTextField();
		txtFirstName.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		txtFirstName.setBounds(360, 160, 97, 20);
		panel.add(txtFirstName);
		txtFirstName.setColumns(10);

		JLabel lbLastName = new JLabel("Tên:");
		lbLastName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbLastName.setBounds(493, 160, 46, 20);
		panel.add(lbLastName);

		txtLastName = new JTextField();
		txtLastName.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		txtLastName.setBounds(549, 160, 121, 20);
		panel.add(txtLastName);
		txtLastName.setColumns(10);

		ButtonGroup bg = new ButtonGroup();

		rbtMale = new JRadioButton("Nam");
		bg.add(rbtMale);
		rbtMale.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rbtMale.setBounds(831, 160, 52, 23);
		panel.add(rbtMale);

		rbtFemale = new JRadioButton("Nữ");
		bg.add(rbtFemale);
		rbtFemale.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rbtFemale.setBounds(902, 160, 41, 23);
		panel.add(rbtFemale);

		JLabel lbId = new JLabel("Mã Nhân Viên:");
		lbId.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbId.setBounds(310, 260, 112, 20);
		panel.add(lbId);

		txtEmail = new JTextField();
		txtEmail.setFont(new Font("SansSerif", Font.PLAIN, 12));
		txtEmail.setBounds(455, 210, 215, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);

		JLabel lbNation = new JLabel("Dân tộc:");
		lbNation.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbNation.setBounds(740, 210, 61, 20);
		panel.add(lbNation);

		JLabel lbEmail = new JLabel("Email:");
		lbEmail.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbEmail.setBounds(310, 210, 46, 20);
		panel.add(lbEmail);

		JLabel lbPhoneNumber = new JLabel("Số Điện Thoại:");
		lbPhoneNumber.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbPhoneNumber.setBounds(310, 310, 105, 20);
		panel.add(lbPhoneNumber);

		JLabel lbIdCard = new JLabel("Số CMND:");
		lbIdCard.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbIdCard.setBounds(310, 360, 78, 20);
		panel.add(lbIdCard);

		JLabel lbDepartmentName = new JLabel("Tên Phòng Ban:");
		lbDepartmentName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbDepartmentName.setBounds(310, 460, 121, 20);
		panel.add(lbDepartmentName);

		JLabel lbPositionName = new JLabel("Tên Chức Vụ: ");
		lbPositionName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbPositionName.setBounds(310, 511, 105, 20);
		panel.add(lbPositionName);

		JLabel lbContractName = new JLabel("Tên HDLD:");
		lbContractName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbContractName.setBounds(740, 310, 78, 20);
		panel.add(lbContractName);

		JLabel lbLiteracyName = new JLabel("Tên TDHV:");
		lbLiteracyName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbLiteracyName.setBounds(740, 360, 97, 20);
		panel.add(lbLiteracyName);

		txtId = new JTextField();
		txtId.setBounds(455, 260, 215, 20);
		panel.add(txtId);
		txtId.setEnabled(false);
		txtId.setColumns(10);

		txtPhoneNumber = new JTextField();
		txtPhoneNumber.setBounds(455, 310, 215, 20);
		panel.add(txtPhoneNumber);
		txtPhoneNumber.setColumns(10);

		txtIdCard = new JTextField();
		txtIdCard.setBounds(455, 360, 215, 20);
		panel.add(txtIdCard);
		txtIdCard.setColumns(10);

		JLabel lbAddress = new JLabel("Địa Chỉ:");
		lbAddress.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbAddress.setBounds(740, 260, 78, 20);
		panel.add(lbAddress);

		ArrayList<String> listPosition = positionBus.getListNameByCond("tencv", "", "");
		Vector<String> list = new Vector<>();
		if (listPosition != null) {
			for (String st : listPosition) {
				list.add(st);
			}
		}
		cbPositionName = new JComboBox(list);
		cbPositionName.setFont(new Font("Tahoma", Font.BOLD, 12));
		cbPositionName.setSelectedIndex(4);
		cbPositionName.setBounds(455, 512, 215, 21);
		panel.add(cbPositionName);

		ArrayList<String> listDepart = departmentBus.getListOneCol("tenphong", "", "");// lấy
																						// danh
																						// sacch1
																						// tên
																						// phòng
		Vector<String> listNameDepart = new Vector<>();
		if (listDepart != null) {
			for (String st : listDepart) {
				listNameDepart.add(st);
			}
		}
		cbDepartmentName = new JComboBox(listNameDepart);
		cbDepartmentName.setFont(new Font("Tahoma", Font.BOLD, 12));
		// cbDepartmentName.setSelectedIndex(4);
		cbDepartmentName.setBounds(455, 460, 215, 21);
		panel.add(cbDepartmentName);

		JLabel lbSex = new JLabel("Giới Tính:");
		lbSex.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbSex.setBounds(740, 160, 82, 20);
		panel.add(lbSex);

		table = new JTable();
		table.setBounds(1340, 699, -1043, -138);
		panel.add(table);

		getContentPane().add(panel);

		JLabel label = new JLabel("");
		label.setBounds(85, 170, 46, 14);
		panel.add(label);

		lbImg = new JLabel("");
		lbImg.setIcon(new ImageIcon("img\\addimage.png"));
		lbImg.setBounds(119, 88, 144, 156);
		panel.add(lbImg);

		btAddImage = new JButton("Thêm Ảnh");
		btAddImage.setFont(new Font("Tahoma", Font.BOLD, 11));
		btAddImage.setBounds(129, 240, 97, 23);
		panel.add(btAddImage);

		lbAdd = new JLabel("Thêm Nhân Viên");
		lbAdd.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbAdd.setIcon(new ImageIcon("img\\add.png"));
		lbAdd.setBounds(310, 608, 147, 48);
		panel.add(lbAdd);

		lbCancel = new JLabel("Hủy");
		lbCancel.setIcon(new ImageIcon("img\\cancel3.png"));
		lbCancel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbCancel.setBounds(704, 608, 97, 48);
		panel.add(lbCancel);

		JLabel lbAddTitle = new JLabel("THÊM NHÂN VIÊN");
		lbAddTitle.setForeground(Color.GREEN);
		lbAddTitle.setFont(new Font("Tahoma", Font.BOLD, 25));
		lbAddTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbAddTitle.setBounds(323, 40, 495, 48);
		panel.add(lbAddTitle);

		lbReturn = new JLabel("");
		lbReturn.setIcon(new ImageIcon("img\\return.png"));
		lbReturn.setBounds(1000, 11, 91, 65);
		panel.add(lbReturn);

		txtNation = new JTextField();
		txtNation.setBounds(850, 210, 168, 20);
		panel.add(txtNation);
		txtNation.setColumns(10);

		txtAddress = new JTextField();
		txtAddress.setBounds(850, 260, 168, 20);
		panel.add(txtAddress);
		txtAddress.setColumns(10);

		JLabel lbBirthday = new JLabel("Ngày Sinh:");
		lbBirthday.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbBirthday.setBounds(310, 410, 112, 20);
		panel.add(lbBirthday);

		cbDay = new JComboBox<String>();
		cbDay.setModel(new DefaultComboBoxModel(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09",
				"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26",
				"27", "28", "29", "30", "31" }));
		cbDay.setBounds(455, 410, 41, 20);
		panel.add(cbDay);

		JLabel lb1 = new JLabel("/");
		lb1.setVerticalAlignment(SwingConstants.TOP);
		lb1.setHorizontalAlignment(SwingConstants.CENTER);
		lb1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lb1.setBounds(493, 410, 30, 37);
		panel.add(lb1);

		cbMonth = new JComboBox<String>();
		cbMonth.setModel(new DefaultComboBoxModel(
				new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
		cbMonth.setBounds(518, 410, 41, 20);
		panel.add(cbMonth);

		JLabel lb2 = new JLabel("/");
		lb2.setVerticalAlignment(SwingConstants.TOP);
		lb2.setHorizontalAlignment(SwingConstants.CENTER);
		lb2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lb2.setBounds(560, 410, 30, 37);
		panel.add(lb2);

		cbYear = new JComboBox<String>();
		cbYear.setModel(new DefaultComboBoxModel(new String[] { "1980", "1981", "1982", "1983", "1984", "1985", "1986",
				"1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998" }));
		cbYear.setBounds(600, 410, 70, 20);
		panel.add(cbYear);

		txtAccount = new JTextField();
		txtAccount.setBounds(850, 460, 168, 20);
		panel.add(txtAccount);
		txtAccount.setColumns(10);

		JLabel lbAccount = new JLabel("Số tài khoản:");
		lbAccount.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbAccount.setBounds(740, 460, 97, 20);
		panel.add(lbAccount);

		ArrayList<String> listContract = new ArrayList<>();
		listContract = contractBus.getListName();
		Vector<String> listContractName = new Vector<>();
		if (listContract != null) {
			for (String st : listContract) {
				listContractName.add(st);
			}
		}
		cbContract = new JComboBox(listContractName);
		cbContract.setFont(new Font("Tahoma", Font.BOLD, 12));
		cbContract.setBounds(850, 310, 168, 20);
		panel.add(cbContract);

		ArrayList<String> listNameLiteracy = new ArrayList<>();
		listNameLiteracy = literacyBus.getListName("tentdhv");
		Vector<String> vtLiteracy = new Vector<>();
		if (listNameLiteracy != null) {
			for (String st : listNameLiteracy) {
				vtLiteracy.add(st);
			}
		}
		cbLiteracyName = new JComboBox(vtLiteracy);
		cbLiteracyName.setFont(new Font("Tahoma", Font.BOLD, 12));
		cbLiteracyName.setBounds(850, 360, 168, 20);
		panel.add(cbLiteracyName);

		txtSpecialized = new JTextField();
		txtSpecialized.setBounds(850, 410, 168, 20);
		panel.add(txtSpecialized);
		txtSpecialized.setColumns(10);

		JLabel lbSpecialized = new JLabel("Chuyên ngành:");
		lbSpecialized.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbSpecialized.setBounds(740, 410, 112, 20);
		panel.add(lbSpecialized);

		clickThem();
		clickXoa();
		clickReturn();
		clickThemAnh();

	}

	public void clickThem() {
		lbAdd.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {
				lbAdd.setForeground(null);

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lbAdd.setForeground(Color.green);

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int respone = JOptionPane.showConfirmDialog(null, "Bạn có chắc chắn thêm nhân viên này?", "Confirm",
						JOptionPane.YES_NO_OPTION);

				if (respone == JOptionPane.YES_OPTION) {
					String sex = "";
					if (rbtMale.isSelected()) {
						sex = "Nam";
					} else {
						sex = "Nữ";
					}
					StringBuilder birthday = new StringBuilder();
					birthday.append(cbYear.getSelectedItem()).append("-").append(cbMonth.getSelectedItem()).append("-")
							.append(cbDay.getSelectedItem());

					String idContract = contractBus.getNewId();// lấy id mới của
																// hopdaonglaodong;
					// chua add hdld
					Date now = new Date();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					ContractDTO contractDto = new ContractDTO();// tao doi tuong
																// hop dong lao
																// dong
					contractDto.setBeginDay(df.format(now));
					contractDto.setIdContract(idContract);
					contractDto.setTypeContract((String) cbContract.getSelectedItem());

					String idLiteracy = "";// id trình độ học vấn
					boolean flag = literacyBus.check("tentdhv", (String) cbLiteracyName.getSelectedItem(),
							"chuyennganh", txtSpecialized.getText());
					@SuppressWarnings("unused")
					boolean flag_2 = contractBus.insert(contractDto);
					if (!flag) {// chưa tồn tại row này
						idLiteracy = literacyBus.createNewId();// tao cai id moi
						// thêm trình đô hoc vấn
						@SuppressWarnings("unused")
						boolean flag_3 = literacyBus.insert(new LiteracyDTO(idLiteracy,
								(String) cbLiteracyName.getSelectedItem(), txtSpecialized.getText()));
					} else {// nếu tồn tại rồi
						idLiteracy = literacyBus.getIdByCondition((String) cbLiteracyName.getSelectedItem(),
								txtSpecialized.getText());
					}
					String id = staffBus.createNewId();// id nhân viên
					NhanVienDTO staff = new NhanVienDTO(id, txtFirstName.getText(), txtLastName.getText(), sex,
							txtAddress.getText(), txtNation.getText(), txtPhoneNumber.getText(), txtAccount.getText(),
							birthday.toString(),
							departmentBus.getIdDepartment((String) cbDepartmentName.getSelectedItem()), idContract,
							positionBus.getIdPositionByName((String) cbPositionName.getSelectedItem()), idLiteracy,
							txtEmail.getText(), txtIdCard.getText(), linkFile);
					if (staffBus.addStaff(staff)) {
						JOptionPane.showMessageDialog(null, "Thêm thành công!");
					} else {
						JOptionPane.showMessageDialog(null, "Thêm thất bại!");
					}
				}
			}
		});
	}

	public void clickXoa() {
		lbCancel.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {
				lbCancel.setForeground(null);

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lbCancel.setForeground(Color.RED);

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new QuanLyNhanSu().setVisible(true);

			}
		});
	}

	public void clickReturn() {
		lbReturn.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new QuanLyNhanSu().setVisible(true);

			}
		});
	}

	public void clickThemAnh() {
		btAddImage.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				ArrayList<String> linkList = staffBus.getListOneCol("hinhanh");
				boolean flag;
				do {
					flag = true;
					JFileChooser fileChooser = new JFileChooser();
					int returnValue = fileChooser.showOpenDialog(contentPane);
					if (returnValue == JFileChooser.APPROVE_OPTION) {
						File selectedFile = fileChooser.getSelectedFile();
						linkFile = selectedFile.getName().toString();
						for (String st : linkList) {
							if (st.equals(linkFile)) {
								JOptionPane.showMessageDialog(null,
										"Ảnh này đã bị trùng với nhân viên khác!! Vui lòng chọn lại ảnh.");
								flag = false;
								break;
							}
						}
					}
				} while (!flag);
				lbImg.setIcon(new ImageIcon("img\\imgnhansu\\" + linkFile));


			}
		});
	}
}