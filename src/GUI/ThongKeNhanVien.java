package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JButton;

public class ThongKeNhanVien extends JFrame {

	private JPanel contentPane;
	private JTextField txtCondition;
	private JComboBox comboBox;
	private JLabel lbOk;
	private JButton btCancel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ThongKeNhanVien frame = new ThongKeNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ThongKeNhanVien() {
		setTitle("Thống kê");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 666, 154);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);
		
		JLabel lbTitle = new JLabel("Thống Kê Nhân Viên: ");
		lbTitle.setFont(new Font("SansSerif", Font.BOLD, 13));
		lbTitle.setBounds(40, 15, 148, 71);
		contentPane.add(lbTitle);
		
		txtCondition = new JTextField();
		txtCondition.setBounds(198, 41, 187, 20);
		contentPane.add(txtCondition);
		txtCondition.setColumns(10);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Theo Giới Tính", "Theo Mức Lương", "Theo Phòng Ban"}));
		comboBox.setBounds(407, 41, 130, 20);
		contentPane.add(comboBox);
		txtCondition.setText((String)comboBox.getSelectedItem());
		comboBox.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				txtCondition.setText((String)comboBox.getSelectedItem());
				
			}
		});
		
		lbOk = new JLabel("");
		lbOk.setHorizontalAlignment(SwingConstants.CENTER);
		lbOk.setIcon(new ImageIcon("img//thongke.png"));
		lbOk.setBounds(547, 27, 53, 48);
		lbOk.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lbOk.setBorder(null);
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				Border border = BorderFactory.createLineBorder(Color.BLACK);
				lbOk.setBorder(border);
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				if((String)comboBox.getSelectedItem()=="Theo Giới Tính")
					new TKGioiTinh().setVisible(true);
				if((String)comboBox.getSelectedItem()=="Theo Mức Lương")
					new TKMucLuong().setVisible(true);
				if((String)comboBox.getSelectedItem()=="Theo Phòng Ban")
					new TKPhongBan().setVisible(true);
			}
		});
		contentPane.add(lbOk);
		
		btCancel = new JButton("Hủy");
		btCancel.setForeground(Color.RED);
		btCancel.setFont(new Font("Tahoma", Font.BOLD, 11));
		btCancel.setBounds(277, 81, 89, 23);
		btCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new MainFrame().setVisible(true);
				
			}
		});
		contentPane.add(btCancel);
	}
}
