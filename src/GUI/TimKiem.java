package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.ChucVuBUS;
import BUS.HopDongLaoDongBUS;
import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import BUS.TrinhDoHocVanBUS;
import DTO.NhanVienDTO;

import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.BevelBorder;

public class TimKiem extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField txtSearch;
	private JButton btnPrint;
	private JLabel lbReturn;
	private DefaultTableModel model;
	private JComboBox cbSearch;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TimKiem frame = new TimKiem();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TimKiem() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\search.png"));
		setTitle("Tìm Kiếm Nhân Viên");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100,  1366, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 361, 1350, 343);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setModel(model=new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"M\u00E3 Nh\u00E2n Vi\u00EAn", "H\u1ECD", "T\u00EAn", "Gi\u1EDBi T\u00EDnh", "Email", "S\u1ED1 \u0110i\u1EC7n Tho\u1EA1i", "S\u1ED1 CMND", "\u0110\u1ECBa Ch\u1EC9", "D\u00E2n t\u1ED9c", "M\u00E3 ph\u00F2ng ban", "M\u00E3 Ch\u1EE9c V\u1EE5", "M\u00E3 HDLD", "M\u00E3TDHV"
			}
		));
		table.setRowHeight(25);
		scrollPane.setViewportView(table);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(100, 0, 1150, 361);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nhập điều kiện tìm:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setBounds(203, 160, 144, 25);
		panel_1.add(lblNewLabel);
		
		txtSearch = new JTextField();
		txtSearch.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtSearch.setBounds(356, 160, 242, 25);
		panel_1.add(txtSearch);
		txtSearch.setColumns(10);
		
		String[] listSearch = { "Tìm kiếm theo Email", "Tìm kiếm theo Mã Chức Vụ", "Tìm kiếm theo Mã Phòng Ban", "Tìm kiếm theo Tên Nhân Viên", "Tìm kiếm theo Mã Nhân Viên" };
		cbSearch = new JComboBox(listSearch);
		cbSearch.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		cbSearch.setSelectedIndex(4);
		cbSearch.setBounds(618, 160, 215, 21);
		panel_1.add(cbSearch);
		
		JLabel lbSearch = new JLabel("");
		lbSearch.setIcon(new ImageIcon("img\\search.png"));
		lbSearch.setBounds(856, 146, 48, 42);
		lbSearch.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				String[] input = new String[2];
				input[1] = txtSearch.getText();
				String cond = (String) cbSearch.getSelectedItem();
				switch (cond) {
				case "Tìm kiếm theo Mã Nhân Viên":
					input[0] = "1";
					break;
				case "Tìm kiếm theo Tên Nhân Viên":
					input[0] = "2";
					break;
				case "Tìm kiếm theo Mã Phòng Ban":
					input[0] = "3";
					break;
				case "Tìm kiếm theo Mã Chức Vụ":
					input[0] = "4";
					break;
				case "Tìm kiếm theo Email":
					input[0] = "5";
					break;	
				}
				NhanVienBUS staffBus=new NhanVienBUS();
				ArrayList<NhanVienDTO> list = staffBus.searchStaff(input);
				Vector<String> row;
				if(!list.isEmpty()){
					for (NhanVienDTO staff : list) {
						row = new Vector<>();
						row.add(staff.getId());
						row.add(staff.getFirstName());
						row.add(staff.getLastName());
						row.add(staff.getSex());
						row.add(staff.getMail());
						row.add(staff.getPhoneNumber());
						row.add(staff.getIdCard());
						row.add(staff.getAddress());
						row.add(staff.getNation());
						row.add(staff.getIdDepartment());
						row.add(staff.getIdPosition());
						row.add(staff.getIdContract());
						row.add(staff.getIdLiteracy());
						model.addRow(row);
					}
					table.setModel(model);
				}else{
					JOptionPane.showMessageDialog(null, "Không tìm thấy nhân viên!! Vui lòng kiểm tra điều kiện.");
				}
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lbSearch.setBorder(null);
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lbSearch.setBorder(new LineBorder(Color.gray));
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		panel_1.add(lbSearch);
		
		btnPrint = new JButton(" XUẤT");
		btnPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		btnPrint.setIcon(new ImageIcon("img\\info.png"));
		btnPrint.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnPrint.setBounds(463, 238, 177, 76);
		panel_1.add(btnPrint);
		
		JLabel lblNewLabel_2 = new JLabel("TÌM KIẾM NHÂN VIÊN");
		lblNewLabel_2.setForeground(Color.BLUE);
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 30));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(231, 26, 624, 76);
		panel_1.add(lblNewLabel_2);
		
		lbReturn = new JLabel("");
		lbReturn.setIcon(new ImageIcon("img\\return.png"));
		lbReturn.setBounds(1049, 11, 75, 65);
		panel_1.add(lbReturn);
		
		clickXoa();
		clickReturn();
	}
	
	public void clickXoa(){
		btnPrint.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				int k = table.getSelectedRow();
				XuatTimKiem search=new XuatTimKiem();
				if (k >= 0) {
					NhanVienBUS staffBus = new NhanVienBUS();
					NhanVienDTO staff = staffBus.getStaff((String) model.getValueAt(k, 0));
					PhongBanBUS departmentBus = new PhongBanBUS();
					ChucVuBUS positionBus = new ChucVuBUS();
					HopDongLaoDongBUS contractBus = new HopDongLaoDongBUS();
					TrinhDoHocVanBUS literacyBus = new TrinhDoHocVanBUS();
					if (staff != null) {
						search.getTxtFirstName().setText(staff.getFirstName());
						search.getTxtLastName().setText(staff.getLastName());
						if(staff.getSex()=="Nữ"){
							search.getRbtFemale().setSelected(true);
						}else{
							search.getRbtMale().setSelected(true);
						}
						search.getTxtMail().setText(staff.getMail());
						search.getTxtNation().setText(staff.getNation());
						search.getTxtAddress().setText(staff.getAddress());
						search.getTxtId().setText(staff.getId());
						search.getTxtPhoneNumber().setText(staff.getPhoneNumber());
						search.getTxtAccount().setText(staff.getAccount());
						search.getTxtIdCard().setText(staff.getIdCard());
						search.getTxtBirthday().setText(staff.getBirthday());
						search.getTxtDepartmentName().setText(departmentBus.getNameDepartment((String) model.getValueAt(k, 9)));
						search.getTxtPositionName().setText(positionBus.getNameById((String) model.getValueAt(k, 10)));
						search.getTxtContractName().setText(contractBus.getName((String) model.getValueAt(k, 11)));
						search.getTxtLiteracyName().setText(literacyBus.getNameById("tentdhv", (String) model.getValueAt(k, 12)));
						search.getTxtSpecialized().setText(literacyBus.getNameById("chuyennganh", (String) model.getValueAt(k, 12)));
						NhanVienBUS bus=new NhanVienBUS();
						search.getLbImg().setIcon(new ImageIcon("img\\imgnhansu\\"+bus.getId("hinhanh", "manv", search.getTxtId().getText())));
						search.setVisible(true);
						setVisible(false);
					}
					
				} else {
					JOptionPane.showMessageDialog(null, "Vui lòng chọn vào dòng chứa nhân viên cần xóa rồi bấm xóa!!!");
				}

			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
				
			}
		});
	}
	
	public void clickReturn(){
		lbReturn.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new QuanLyNhanSu().setVisible(true);
				
			}
		});
	}
}
