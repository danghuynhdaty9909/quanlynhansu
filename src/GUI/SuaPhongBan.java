package GUI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import DAO.NhanVienDAO;
import DTO.DepartmentDTO;

public class SuaPhongBan extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtBoss;
	private JTextField txtAddress;
	private JButton btGetBoss;
	private JButton btCancel;
	private JButton btFix;

	private static String oldName = "";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SuaPhongBan frame = new SuaPhongBan();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SuaPhongBan() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\sua.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 659, 415);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);

		JLabel lbName = new JLabel("Tên Phòng Ban:");
		lbName.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbName.setBounds(95, 110, 125, 25);
		contentPane.add(lbName);

		JLabel lbBoss = new JLabel("Trưởng Phòng:");
		lbBoss.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbBoss.setBounds(95, 155, 125, 25);
		contentPane.add(lbBoss);

		JLabel lbAddress = new JLabel("Địa Chỉ Phòng:");
		lbAddress.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbAddress.setBounds(95, 200, 125, 25);
		contentPane.add(lbAddress);

		txtName = new JTextField();
		txtName.setBounds(270, 110, 257, 25);
		contentPane.add(txtName);
		txtName.setColumns(10);

		txtBoss = new JTextField();
		txtBoss.setBounds(270, 155, 189, 25);
		contentPane.add(txtBoss);
		txtBoss.setColumns(10);

		txtAddress = new JTextField();
		txtAddress.setBounds(270, 200, 257, 25);
		contentPane.add(txtAddress);
		txtAddress.setColumns(10);

		JLabel lbTitle = new JLabel("Sửa Phòng Ban");
		lbTitle.setForeground(Color.BLUE);
		lbTitle.setBackground(new Color(0, 100, 0));
		lbTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbTitle.setBounds(10, 29, 600, 33);
		contentPane.add(lbTitle);

		btFix = new JButton("Sửa");
		btFix.setBackground(Color.WHITE);
		btFix.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btFix.setBounds(202, 289, 86, 25);
		contentPane.add(btFix);

		btCancel = new JButton("Hủy");
		btCancel.setBackground(Color.WHITE);
		btCancel.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btCancel.setBounds(424, 289, 77, 25);
		btCancel.addActionListener(this);
		contentPane.add(btCancel);

		btGetBoss = new JButton("New button");
		btGetBoss.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btGetBoss.setBackground(Color.WHITE);
		btGetBoss.setBounds(478, 155, 49, 25);
		contentPane.add(btGetBoss);
		btGetBoss.addActionListener(this);
		btFix.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btCancel) {
			setVisible(false);
			new QuanLyPhongBan().setVisible(true);
		}
		if (e.getSource() == btGetBoss) {
			DSNhanVienTheoPhong list = new DSNhanVienTheoPhong();
			QuanLyPhongBan.getCurDepart().setAddress(txtAddress.getText());// lấy
																			// địa
																			// chỉ
																			// mới
																			// sửa
			QuanLyPhongBan.getCurDepart().setName(txtName.getText());// lấy tên
																		// phòng
																		// mới
																		// sửa
			list.setVisible(true);
		}
		if (e.getSource() == btFix) {
			PhongBanBUS busDepart = new PhongBanBUS();
			NhanVienBUS busStaff = new NhanVienBUS();
			QuanLyPhongBan.getCurDepart().setAddress(txtAddress.getText());
			QuanLyPhongBan.getCurDepart().setName(txtName.getText());
			QuanLyPhongBan.getCurDepart().setIdBoss(DSNhanVienTheoPhong.getNewIdBoss());//mã truong phong moi
			String idPosition = DSNhanVienTheoPhong.getIdPosition();// lấy chức
																	// vụ cũa
																	// thằng
																	// nhân viên
																	// mới
			String oldIdPosition = busStaff.getId("macv", "manv", QuanLyPhongBan.getIdOldBoss());// lấy
																									// chức
																									// vụ
																									// chủa
																									// thằng
																									// truong
																									// phòng
																									// cu;
			if (busDepart.update(QuanLyPhongBan.getCurDepart())
					&& busStaff.updateOneCol("macv", oldIdPosition, "manv", QuanLyPhongBan.getCurDepart().getIdBoss())
					&& busStaff.updateOneCol("macv", idPosition, "manv", QuanLyPhongBan.getIdOldBoss())) {
				JOptionPane.showMessageDialog(null, "Update thành công!!!");
			} else {
				JOptionPane.showMessageDialog(null, "Update thất bại!!!");
			}
		}

	}

	public JTextField getTxtName() {
		return txtName;
	}

	public void setTxtName(JTextField txtName) {
		this.txtName = txtName;
	}

	public JTextField getTxtBoss() {
		return txtBoss;
	}

	public void setTxtBoss(JTextField txtBoss) {
		this.txtBoss = txtBoss;
	}

	public JTextField getTxtAddress() {
		return txtAddress;
	}

	public void setTxtAddress(JTextField txtAddress) {
		this.txtAddress = txtAddress;
	}

	public JButton getBtGetBoss() {
		return btGetBoss;
	}

	public void setBtGetBoss(JButton btGetBoss) {
		this.btGetBoss = btGetBoss;
	}

	public JButton getBtCancel() {
		return btCancel;
	}

	public void setBtCancel(JButton btCancel) {
		this.btCancel = btCancel;
	}

	public JButton getBtFix() {
		return btFix;
	}

	public void setBtFix(JButton btFix) {
		this.btFix = btFix;
	}

}
