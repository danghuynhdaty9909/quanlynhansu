package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import BUS.ChucVuBUS;
import BUS.HopDongLaoDongBUS;
import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import DTO.ContractDTO;
import DTO.DepartmentDTO;
import DTO.NhanVienDTO;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class XuatSuaNV extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtEmail;
	private JTextField txtId;
	private JTextField txtPhoneNumber;
	private JTextField txtIdCard;
	private JTable table;
	private JTable table_1;
	private JTable table_2;
	private JLabel lbCancel;
	private JLabel lbOK;
	private JTextField txtNation;
	private JTextField txtAddress;
	private JTextField txtAccount;
	private JButton btTakeImg;
	private JLabel lbImg;
	private String linkAnh;
	private JRadioButton rbtMale;
	private JRadioButton rbtFemale;
	@SuppressWarnings("rawtypes")
	private JComboBox cbDay;
	@SuppressWarnings("rawtypes")
	private JComboBox cbMonth;
	@SuppressWarnings("rawtypes")
	private JComboBox cbYear;
	@SuppressWarnings("rawtypes")
	private JComboBox cbPositionName;
	@SuppressWarnings("rawtypes")
	private JComboBox cbDepartmentName;
	@SuppressWarnings("rawtypes")
	private JComboBox cbContractName;
	private NhanVienBUS busStaff = new NhanVienBUS();
	private HopDongLaoDongBUS busContract = new HopDongLaoDongBUS();
	private ChucVuBUS busPosition = new ChucVuBUS();
	private PhongBanBUS busDepartment = new PhongBanBUS();// create object

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					XuatSuaNV frame = new XuatSuaNV();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public XuatSuaNV() {
		setTitle("Sửa Nhân Viên");
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\officer.png"));
		setBounds(100, 100, 1366, 768);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setForeground(Color.BLACK);
		panel.setBounds(149, 11, 1055, 729);
		panel.setLayout(null);

		JLabel lbTitle = new JLabel("Sửa Thông Tin Nhân Viên");
		lbTitle.setForeground(Color.CYAN);
		lbTitle.setFont(new Font("SansSerif", Font.BOLD, 30));
		lbTitle.setBounds(377, 35, 397, 57);
		panel.add(lbTitle);

		JLabel lbFirstName = new JLabel("Họ:");
		lbFirstName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbFirstName.setBounds(200, 245, 30, 20);
		panel.add(lbFirstName);

		txtFirstName = new JTextField();
		txtFirstName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtFirstName.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		txtFirstName.setBounds(250, 245, 97, 23);
		panel.add(txtFirstName);
		txtFirstName.setColumns(10);

		JLabel lbLastName = new JLabel("Tên:");
		lbLastName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbLastName.setBounds(377, 245, 46, 20);
		panel.add(lbLastName);

		txtLastName = new JTextField();
		txtLastName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtLastName.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		txtLastName.setBounds(443, 245, 121, 23);
		panel.add(txtLastName);
		txtLastName.setColumns(10);

		ButtonGroup bg = new ButtonGroup();

		rbtMale = new JRadioButton("Nam");
		bg.add(rbtMale);
		rbtMale.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rbtMale.setBounds(743, 207, 52, 23);
		panel.add(rbtMale);

		rbtFemale = new JRadioButton("Nữ");
		bg.add(rbtFemale);
		rbtFemale.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rbtFemale.setBounds(814, 207, 41, 23);
		panel.add(rbtFemale);

		JLabel lbId = new JLabel("Mã Nhân Viên:");
		lbId.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbId.setBounds(200, 207, 112, 20);
		panel.add(lbId);

		txtEmail = new JTextField();
		txtEmail.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtEmail.setFont(new Font("SansSerif", Font.PLAIN, 12));
		txtEmail.setBounds(349, 284, 215, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);

		JLabel lbNation = new JLabel("Dân tộc:");
		lbNation.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbNation.setBounds(635, 245, 61, 20);
		panel.add(lbNation);

		JLabel lbEmail = new JLabel("Email:");
		lbEmail.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbEmail.setBounds(200, 283, 46, 20);
		panel.add(lbEmail);

		JLabel lbPhoneNumber = new JLabel("Số Điện Thoại:");
		lbPhoneNumber.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbPhoneNumber.setBounds(200, 324, 105, 20);
		panel.add(lbPhoneNumber);

		JLabel lbIdCard = new JLabel("Số CMND:");
		lbIdCard.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbIdCard.setBounds(200, 363, 78, 20);
		panel.add(lbIdCard);

		JLabel lbDepartmentName = new JLabel("Tên Phòng Ban:");
		lbDepartmentName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbDepartmentName.setBounds(200, 442, 121, 20);
		panel.add(lbDepartmentName);

		JLabel lbPositionName = new JLabel("Tên Chức Vụ: ");
		lbPositionName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbPositionName.setBounds(635, 406, 105, 20);
		panel.add(lbPositionName);

		JLabel lbContractName = new JLabel("Tên HĐLĐ:");
		lbContractName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbContractName.setBounds(635, 363, 78, 20);
		panel.add(lbContractName);

		txtId = new JTextField();
		txtId.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtId.setBounds(349, 209, 215, 20);
		panel.add(txtId);
		txtId.setColumns(10);

		txtPhoneNumber = new JTextField();
		txtPhoneNumber.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtPhoneNumber.setBounds(349, 324, 215, 20);
		panel.add(txtPhoneNumber);
		txtPhoneNumber.setColumns(10);

		txtIdCard = new JTextField();
		txtIdCard.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtIdCard.setBounds(349, 363, 215, 20);
		panel.add(txtIdCard);
		txtIdCard.setColumns(10);

		JLabel lbAddress = new JLabel("Địa chỉ:");
		lbAddress.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbAddress.setBounds(635, 285, 78, 20);
		panel.add(lbAddress);

		cbPositionName = new JComboBox();
		cbPositionName.setFont(new Font("Tahoma", Font.BOLD, 12));
		ArrayList<String> listPositionName = busPosition.getListNameByCond("tencv", "", "");
		Vector<String> namePosition = new Vector<>();
		if (!listPositionName.isEmpty()) {
			for (String st : listPositionName) {
				namePosition.add(st);
			}
		}
		cbPositionName.setModel(new DefaultComboBoxModel(namePosition));
		cbPositionName.setSelectedIndex(4);
		cbPositionName.setBounds(743, 406, 215, 21);
		panel.add(cbPositionName);

		cbDepartmentName = new JComboBox();
		cbDepartmentName.setFont(new Font("Tahoma", Font.BOLD, 12));
		ArrayList<String> listDepartName = busDepartment.getListOneCol("tenphong", "", "");// get
																							// name
																							// department
																							// list
		Vector<String> name = new Vector<>();
		if (!listDepartName.isEmpty()) {
			for (String st : listDepartName) {
				name.add(st);
			}
		}
		cbDepartmentName.setModel(new DefaultComboBoxModel(name));
		cbDepartmentName.setSelectedIndex(4);
		cbDepartmentName.setBounds(349, 442, 215, 21);
		panel.add(cbDepartmentName);

		JLabel lbSex = new JLabel("Giới Tính:");
		lbSex.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbSex.setBounds(635, 207, 82, 20);
		panel.add(lbSex);

		table = new JTable();
		table.setBounds(1340, 699, -1043, -138);
		panel.add(table);

		getContentPane().add(panel);

		lbCancel = new JLabel("Hủy");
		lbCancel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbCancel.setIcon(new ImageIcon("img\\cancel.png"));
		lbCancel.setBounds(659, 570, 136, 72);
		panel.add(lbCancel);

		lbOK = new JLabel("Đồng Ý");
		lbOK.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbOK.setIcon(new ImageIcon("img\\accept.png"));
		lbOK.setBounds(281, 570, 121, 72);
		panel.add(lbOK);

		lbImg = new JLabel("");
		lbImg.setBorder(new LineBorder(Color.LIGHT_GRAY));
		linkAnh = SuaNhanVien.getOldStaff().getPathImg();
		lbImg.setIcon(new ImageIcon(linkAnh));
		lbImg.setBounds(21, 74, 121, 123);
		panel.add(lbImg);

		btTakeImg = new JButton("Đổi ảnh");
		btTakeImg.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		btTakeImg.setBounds(38, 208, 89, 23);
		panel.add(btTakeImg);

		txtNation = new JTextField();
		txtNation.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtNation.setBounds(743, 247, 215, 20);
		panel.add(txtNation);
		txtNation.setColumns(10);

		txtAddress = new JTextField();
		txtAddress.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtAddress.setBounds(743, 285, 215, 20);
		panel.add(txtAddress);
		txtAddress.setColumns(10);

		JLabel lbBirthday = new JLabel("Ngày Sinh: ");
		lbBirthday.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbBirthday.setBounds(200, 403, 136, 23);
		panel.add(lbBirthday);

		cbDay = new JComboBox();
		cbDay.setModel(new DefaultComboBoxModel(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09",
				"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26",
				"27", "28", "29", "30", "31" }));
		cbDay.setBounds(349, 406, 41, 20);
		panel.add(cbDay);

		JLabel lblNewLabel_2 = new JLabel("/");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(400, 403, 30, 28);
		panel.add(lblNewLabel_2);

		cbMonth = new JComboBox();
		cbMonth.setModel(new DefaultComboBoxModel(
				new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
		cbMonth.setBounds(429, 406, 46, 20);
		panel.add(cbMonth);

		JLabel label = new JLabel("/");
		label.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(480, 403, 23, 28);
		panel.add(label);

		JLabel lbAccount = new JLabel("Số Tài Khoản:");
		lbAccount.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbAccount.setBounds(635, 324, 100, 14);
		panel.add(lbAccount);

		txtAccount = new JTextField();
		txtAccount.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtAccount.setBounds(743, 324, 215, 20);
		panel.add(txtAccount);
		txtAccount.setColumns(10);

		cbYear = new JComboBox();
		cbYear.setModel(new DefaultComboBoxModel(
				new String[] { "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990",
						"1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000" }));
		cbYear.setBounds(503, 406, 61, 20);
		panel.add(cbYear);

		cbContractName = new JComboBox();
		cbContractName.setFont(new Font("Tahoma", Font.BOLD, 12));
		ArrayList<String> listContractName = busContract.getList("loaihopdong");
		Vector<String> nameList = new Vector<>();
		if (!listContractName.isEmpty()) {
			for (String st : listContractName) {
				nameList.add(st);
			}
		}
		cbContractName.setModel(new DefaultComboBoxModel<>(nameList));
		cbContractName.setBounds(743, 363, 215, 20);
		panel.add(cbContractName);

		clickCancel();
		clickChangeImg();
		clickOK();

	}

	public void clickOK() {
		lbOK.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {
				lbOK.setForeground(null);

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lbOK.setForeground(Color.green);

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int respone = JOptionPane.showConfirmDialog(null, "Bạn có chắc chắn muốn lưu thông tin?", "Confirm",
						JOptionPane.YES_NO_OPTION);
				if (respone == JOptionPane.YES_OPTION) {
					Date now = new Date();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					String beginDay = df.format(now);
					ContractDTO contract = new ContractDTO(SuaNhanVien.getOldStaff().getIdContract(),
							(String) cbContractName.getSelectedItem(), beginDay);
					@SuppressWarnings("unused")
					boolean flag_1 = busContract.update(contract);// update
																	// hopdonglaodong
					String birthday = (String) cbYear.getSelectedItem() + "-" + (String) cbMonth.getSelectedItem() + "-"
							+ (String) cbDay.getSelectedItem();
					String idDepart = busDepartment.getIdDepartment((String) cbDepartmentName.getSelectedItem());
					String idPosition = busPosition.getIdPositionByName((String) cbPositionName.getSelectedItem());
					if (idDepart.equals("P0") && idPosition.equals("TP")) {// nếu
																			// là
																			// phòng
																			// giám
																			// đốc
																			// và
																			// là
																			// trưởng
																			// phòng
																			// thì
																			// mã
																			// chức
																			// vụ
																			// là
																			// CEO
						idPosition = "CEO";
					}
					if (idPosition.equals("CEO") || idPosition.equals("TP")) {
						String address = busDepartment.getAddress(idDepart);
						String oldIdBoss = busDepartment.getIdBoss(idDepart);
						// tao đối tượng phòng ban trường hợp đổi trưởng phòng
						DepartmentDTO dto = new DepartmentDTO(idDepart, (String) cbDepartmentName.getSelectedItem(),
								getTxtId().getText(), address);
						@SuppressWarnings("unused")
						boolean flag_2 = busDepartment.update(dto);// update
						@SuppressWarnings("unused")
						boolean flag_3 = busStaff.updateOneCol("macv", SuaNhanVien.getOldStaff().getIdPosition(),
								"manv", oldIdBoss);
					}
					NhanVienDTO newStaff = new NhanVienDTO(getTxtId().getText(), getTxtFirstName().getText(),
							getTxtLastName().getText(), getRbtFemale().isSelected() ? "Nữ" : "Nam",
							getTxtAddress().getText(), getTxtNation().getText(), getTxtPhoneNumber().getText(),
							getTxtAccount().getText(), birthday, idDepart, SuaNhanVien.getOldStaff().getIdContract(),
							idPosition, SuaNhanVien.getOldStaff().getIdLiteracy(), txtEmail.getText(),
							txtIdCard.getText(), linkAnh);
					if (busStaff.updateStaff(newStaff)) {
						JOptionPane.showMessageDialog(null, "Sửa thành công!!!");
					} else {
						JOptionPane.showMessageDialog(null, "Sửa thất bại!!!");
					}

				}

			}

		});

	}

	public void clickCancel() {
		lbCancel.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				lbCancel.setForeground(null);

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lbCancel.setForeground(Color.red);

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new SuaNhanVien().setVisible(true);

			}
		});
	}

	public void clickChangeImg() {
		btTakeImg.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				ArrayList<String> linkList = busStaff.getListOneCol("hinhanh");
				boolean flag;
				do {
					flag = true;
					JFileChooser fileChooser = new JFileChooser();
					int returnValue = fileChooser.showOpenDialog(contentPane);
					if (returnValue == JFileChooser.APPROVE_OPTION) {
						File selectedFile = fileChooser.getSelectedFile();
						linkAnh = selectedFile.getName().toString();
						for (String st : linkList) {
							if (st.equals(linkAnh)) {
								JOptionPane.showMessageDialog(null,
										"Ảnh này đã bị trùng với nhân viên khác!! Vui lòng chọn lại ảnh.");
								flag = false;
								break;
							}
						}
					}
				} while (!flag);
				lbImg.setIcon(new ImageIcon("img\\imgnhansu\\" + linkAnh));

			}
		});
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JTextField getTxtFirstName() {
		return txtFirstName;
	}

	public void setTxtFirstName(JTextField txtFirstName) {
		this.txtFirstName = txtFirstName;
	}

	public JTextField getTxtLastName() {
		return txtLastName;
	}

	public void setTxtLastName(JTextField txtLastName) {
		this.txtLastName = txtLastName;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}

	public JTextField getTxtId() {
		return txtId;
	}

	public void setTxtId(JTextField txtId) {
		this.txtId = txtId;
	}

	public JTextField getTxtPhoneNumber() {
		return txtPhoneNumber;
	}

	public void setTxtPhoneNumber(JTextField txtPhoneNumber) {
		this.txtPhoneNumber = txtPhoneNumber;
	}

	public JTextField getTxtIdCard() {
		return txtIdCard;
	}

	public void setTxtIdCard(JTextField txtIdCard) {
		this.txtIdCard = txtIdCard;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JTable getTable_1() {
		return table_1;
	}

	public void setTable_1(JTable table_1) {
		this.table_1 = table_1;
	}

	public JTable getTable_2() {
		return table_2;
	}

	public void setTable_2(JTable table_2) {
		this.table_2 = table_2;
	}

	public JLabel getLbCancel() {
		return lbCancel;
	}

	public void setLbCancel(JLabel lbCancel) {
		this.lbCancel = lbCancel;
	}

	public JLabel getLbOK() {
		return lbOK;
	}

	public void setLbOK(JLabel lbOk) {
		this.lbOK = lbOk;
	}

	public JTextField getTxtNation() {
		return txtNation;
	}

	public void setTxtNation(JTextField txtNation) {
		this.txtNation = txtNation;
	}

	public JTextField getTxtAddress() {
		return txtAddress;
	}

	public void setTxtAddress(JTextField txtAddress) {
		this.txtAddress = txtAddress;
	}

	public JTextField getTxtAccount() {
		return txtAccount;
	}

	public void setTxtAccount(JTextField txtAccount) {
		this.txtAccount = txtAccount;
	}

	public JButton getBtTakeImg() {
		return btTakeImg;
	}

	public void setBtTakeImg(JButton btTakeImg) {
		this.btTakeImg = btTakeImg;
	}

	public JLabel getLbImg() {
		return lbImg;
	}

	public void setLbImg(JLabel lbImg) {
		this.lbImg = lbImg;
	}

	public String getLinkAnh() {
		return linkAnh;
	}

	public void setLinkAnh(String linkAnh) {
		this.linkAnh = linkAnh;
	}

	public JRadioButton getRbtMale() {
		return rbtMale;
	}

	public void setRbtMale(JRadioButton rbtMale) {
		this.rbtMale = rbtMale;
	}

	public JRadioButton getRbtFemale() {
		return rbtFemale;
	}

	public void setRbtFemale(JRadioButton rbtFemale) {
		this.rbtFemale = rbtFemale;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getCbDay() {
		return cbDay;
	}

	@SuppressWarnings("rawtypes")
	public void setCbDay(JComboBox cbDay) {
		this.cbDay = cbDay;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getCbMonth() {
		return cbMonth;
	}

	@SuppressWarnings("rawtypes")
	public void setCbMonth(JComboBox cbMonth) {
		this.cbMonth = cbMonth;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getCbYear() {
		return cbYear;
	}

	@SuppressWarnings("rawtypes")
	public void setCbYear(JComboBox cbYear) {
		this.cbYear = cbYear;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getCbPositionName() {
		return cbPositionName;
	}

	public void setCbPositionName(@SuppressWarnings("rawtypes") JComboBox cbPositionName) {
		this.cbPositionName = cbPositionName;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getCbDepartmentName() {
		return cbDepartmentName;
	}

	public void setCbDepartmentName(@SuppressWarnings("rawtypes") JComboBox cbDepartmentName) {
		this.cbDepartmentName = cbDepartmentName;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getCbContractName() {
		return cbContractName;
	}

	@SuppressWarnings("rawtypes")
	public void setCbContractName(JComboBox cbContractName) {
		this.cbContractName = cbContractName;
	}
}
