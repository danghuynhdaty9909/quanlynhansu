package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

@SuppressWarnings("serial")
public class QuanLyNhanSu extends JFrame implements ActionListener, MouseListener {

	private JPanel contentPane;
	private JLabel lbIThemNS;
	private JLabel lbIXoaNS;
	private JLabel lbISuaNS;
	private JLabel lbIXuatNS;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					QuanLyNhanSu frame = new QuanLyNhanSu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public QuanLyNhanSu() {
		setIconImage(
				Toolkit.getDefaultToolkit().getImage("img\\officer.png"));
		setTitle("Quản Lý Nhân Sự");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);

		lbIThemNS = new JLabel("Thêm Nhân Viên");
		lbIThemNS.setBounds(182, 38, 335, 314);
		lbIThemNS.setPreferredSize(new Dimension(300, 300));
		lbIThemNS.setVerticalTextPosition(SwingConstants.BOTTOM);
		lbIThemNS.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbIThemNS.setIcon(new ImageIcon("img\\themnhansu.png"));
		lbIThemNS.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lbIThemNS);

		lbIXoaNS = new JLabel("Xóa Nhân Viên");
		lbIXoaNS.setBounds(182, 415, 335, 271);
		lbIXoaNS.setVerticalTextPosition(SwingConstants.BOTTOM);
		lbIXoaNS.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbIXoaNS.setIcon(new ImageIcon("img\\xoanhansu.png"));
		lbIXoaNS.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lbIXoaNS);

		lbISuaNS = new JLabel("Sửa Nhân Viên");
		lbISuaNS.setBounds(770, 38, 335, 300);
		lbISuaNS.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbISuaNS.setVerticalTextPosition(SwingConstants.BOTTOM);
		lbISuaNS.setHorizontalAlignment(SwingConstants.CENTER);
		lbISuaNS.setIcon(new ImageIcon("img\\suanhansu.png"));
		contentPane.add(lbISuaNS);

		lbIXuatNS = new JLabel("Tìm Kiếm");
		lbIXuatNS.setBounds(770, 415, 335, 271);
		lbIXuatNS.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbIXuatNS.setVerticalTextPosition(SwingConstants.BOTTOM);
		lbIXuatNS.setIcon(new ImageIcon("img\\xuatnhansu.png"));
		lbIXuatNS.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lbIXuatNS);
		
		JLabel lbReturn = new JLabel("");
		lbReturn.setIcon(new ImageIcon("img\\return.png"));
		lbReturn.setBounds(1255, 21, 85, 61);
		lbReturn.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new MainFrame().setVisible(true);
				
			}
		});
		contentPane.add(lbReturn);
		
		them();
		xoa();
		sua();
		tim();
	}
	
	public void them(){
		lbIThemNS.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				setVisible(false);
				new ThemNhanVien().setVisible(true);
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lbIThemNS.setForeground(Color.BLACK);
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lbIThemNS.setForeground(Color.CYAN);
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public void xoa(){
		lbIXoaNS.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lbIXoaNS.setForeground(Color.BLACK);
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lbIXoaNS.setForeground(Color.CYAN);
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new XoaNhanVien().setVisible(true);
				
			}
		});
	}
	
	public void sua(){
		lbISuaNS.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
			
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lbISuaNS.setForeground(Color.BLACK);
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lbISuaNS.setForeground(Color.CYAN);
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new SuaNhanVien().setVisible(true);
				
			}
		});
	}
	
	public void tim(){
		lbIXuatNS.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				lbIXuatNS.setForeground(Color.BLACK);
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				lbIXuatNS.setForeground(Color.CYAN);				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new TimKiem().setVisible(true);
				
			}
		});
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}

