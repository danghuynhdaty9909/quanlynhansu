package GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.ChucVuBUS;
import BUS.HopDongLaoDongBUS;
import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import BUS.TrinhDoHocVanBUS;
import DAO.HopDongLaoDongDAO;
import DAO.TrinhDoHocVanDAO;
import DTO.DepartmentDTO;
import DTO.NhanVienDTO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JButton;

public class ChonNhanVien extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTable table;
	private DefaultTableModel model;
	private JButton btOk;
	private JButton btClose;
	private static String newIdBoss;
	private static String nameBoss = "";
	PhongBanBUS bus = new PhongBanBUS();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChonNhanVien frame = new ChonNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ChonNhanVien() {
		//nimbus look and feel
				try {
					for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
						if ("Windows".equals(info.getName())) {
							UIManager.setLookAndFeel(info.getClassName());
							break;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 93, 1330, 387);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setFont(new Font("SansSerif", Font.BOLD, 12));
		table.setModel(model=new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"M\u00E3 Nh\u00E2n Vi\u00EAn", "H\u1ECD", "T\u00EAn", "Gi\u1EDBi T\u00EDnh", "Email", "S\u1ED1 \u0110i\u1EC7n Tho\u1EA1i", "S\u1ED1 CMND", "Qu\u00EA Qu\u00E1n", "M\u00E3 ph\u00F2ng ban", "M\u00E3 ch\u1EE9c v\u1EE5", "M\u00E3 h\u1EE3p \u0111\u1ED3ng", "M\u00E3 TDHV"
			}
		));
		scrollPane.setViewportView(table);

		JLabel lblNewLabel = new JLabel("Danh Sách Nhân Viên");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 30));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(89, 11, 1163, 71);
		contentPane.add(lblNewLabel);

		btOk = new JButton("Chọn Nhân Viên");
		btOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int row = table.getSelectedRow();
				String id = (String) table.getValueAt(row, 0);
				
				NhanVienBUS nvb = new NhanVienBUS();
//				 int dialogButton = JOptionPane.YES_NO_OPTION;
//				int dialogResult = JOptionPane.showConfirmDialog (null, "Bạn muốn chọn nhân viên "+table.getValueAt(row, 1)+" "+table.getValueAt(row, 2)+"?","Thông báo",dialogButton);
//				if(dialogResult == JOptionPane.YES_OPTION){
				
//				}
				NhanVienBUS.setSttNhanVien(nvb.getStaff(id));
				dispose();
			}
		});
		btOk.setBounds(849, 535, 131, 23);
		contentPane.add(btOk);

		btClose = new JButton("Đóng");
		btClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btClose.setBounds(425, 535, 89, 23);
		contentPane.add(btClose);
		
		loadData();
	}

	public void loadData() {
		NhanVienBUS staffBus=new NhanVienBUS();
		ArrayList<NhanVienDTO> list = staffBus.loadDataByCondition("", "");
		Vector<String> row;
		
		for (NhanVienDTO staff : list) {
			row = new Vector<>();
			row.add(staff.getId());
			row.add(staff.getFirstName());
			row.add(staff.getLastName());
			row.add(staff.getSex());
			row.add(staff.getMail());
			row.add(staff.getPhoneNumber());
			row.add(staff.getIdCard());
			row.add(staff.getAddress());
			row.add(staff.getIdDepartment());
			row.add(staff.getIdPosition());
			row.add(staff.getIdContract());
			row.add(staff.getIdLiteracy());
			model.addRow(row);
		}
		table.setModel(model);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	

		
}
